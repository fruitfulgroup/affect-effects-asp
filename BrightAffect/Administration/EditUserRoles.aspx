﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="EditUserRoles.aspx.cs" Inherits="BrightAffect.Administration.EditUserRoles" %>


<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <title>Edit User Roles</title>
    <asp:PlaceHolder runat="server">
        <%: Scripts.Render("~/bundles/modernizr") %>
    </asp:PlaceHolder>
    <webopt:BundleReference runat="server" Path="~/Content/css" />
    <link href="~/favicon.ico" rel="shortcut icon" type="image/x-icon" />
</head>
<body>
    <form id="form1" runat="server">

    <%--<telerik:RadAjaxManager runat="server" >
        <AjaxSettings>
            <telerik:AjaxSetting AjaxControlID="Button_NewRole">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="RadGrid_Roles" LoadingPanelID=""></telerik:AjaxUpdatedControl>
                </UpdatedControls>
            </telerik:AjaxSetting>
            </AjaxSettings>
    </telerik:RadAjaxManager>
    <telerik:RadScriptManager ID="RadScriptManager1" runat="server"></telerik:RadScriptManager>--%>

    <asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>

    <div style="padding: 30px; height: 400px; width:350px; overflow:no-display" >
        
        <div class="row">
            <div class="col-md-12">
                <section id="passwordForm">
                    <asp:PlaceHolder runat="server" ID="setPassword">

                        <div class="form-horizontal">
                            <h4>Edit User Roles</h4>
                            <hr />
                            
                            <div class="form-group">
                                <asp:Label ID="Label3" runat="server" AssociatedControlID="TextBox_Username" CssClass="col-md-2 control-label">Username</asp:Label>
                                <div class="col-md-10">
                                    <asp:TextBox runat="server" ID="TextBox_Username" CssClass="form-control" ReadOnly="true"/>
                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator6" runat="server" ControlToValidate="TextBox_Username"
                                        CssClass="text-danger" ErrorMessage="The username field is required."
                                        Display="Dynamic" ValidationGroup="SetPassword" />
                                </div>
                            </div>

                            <div class="form-group">
                                <div class="col-md-12">
                                    <asp:Button ID="Button_NewRole" runat="server" Class="btn btn-info" Text="Add Role" ValidationGroup="SetPassword" OnClick="OnClick_NewRole"  />
                                </div>
                            </div>

                            <div class="form-group">
                                <telerik:RadGrid ID="RadGrid_Roles" runat="server" CellSpacing="-1" DataSourceID="SqlDataSource_UserRoles" GridLines="None" GroupPanelPosition="Top" AllowAutomaticDeletes="True" AllowAutomaticInserts="True" AllowAutomaticUpdates="True">
                                    <MasterTableView DataKeyNames="ID,UserId,RoleID" DataSourceID="SqlDataSource_UserRoles" AutoGenerateColumns="False">
                                        <Columns>
                                            <telerik:GridDropDownColumn DataField="RoleId" DataSourceID="SqlDataSource_Roles" ListTextField="Name" 
                                                ListValueField="ID" AllowFiltering="True" HeaderText="Roles" UniqueName="RoleId" AllowVirtualScrolling="True" AllowAutomaticLoadOnDemand="true" AndCurrentFilterFunction="StartsWith" ConvertEmptyStringToNull="False"></telerik:GridDropDownColumn>
                                            <telerik:GridButtonColumn CommandName="Delete" Text="Delete Role" UniqueName="DeleteButton" ConfirmText="Are you sure you want to delete this Role?" ConfirmDialogType="RadWindow" Reorderable="False" ItemStyle-Width="120px" />
                                        </Columns>
                                    </MasterTableView>
                                </telerik:RadGrid>
                                
                            </div>
                                                        
                        </div>
                    </asp:PlaceHolder>

                </section>
            </div>
        </div>
        </div>

        <asp:SqlDataSource runat="server" ID="SqlDataSource_UserRoles" ConnectionString='<%$ ConnectionStrings:DefaultConnection %>' 
            SelectCommand="SELECT * FROM [AspNetUserRoles] WHERE ([UserId] = @UserId) ORDER BY RoleId" 
            DeleteCommand="DELETE FROM AspNetUserRoles WHERE (Id = @ID)" 
            InsertCommand="IF NOT EXISTS(SELECT * FROM [AspNetUserRoles] WHERE [UserId] = @UserId AND RoleId = @RoleId) BEGIN INSERT INTO AspNetUserRoles(UserId, RoleId) VALUES (@UserId, @RoleId) END" >
            <DeleteParameters>
                <asp:Parameter Name="ID"></asp:Parameter>
            </DeleteParameters>
            <InsertParameters>
                <asp:QueryStringParameter QueryStringField="UserId" Name="UserId" Type="String"></asp:QueryStringParameter>
                <asp:Parameter Name="RoleId"></asp:Parameter>
            </InsertParameters>
            <SelectParameters>
                <asp:QueryStringParameter QueryStringField="UserId" Name="UserId" Type="String"></asp:QueryStringParameter>
            </SelectParameters>
        </asp:SqlDataSource>

        <asp:SqlDataSource ID="SqlDataSource_Roles" runat="server" ConnectionString='<%$ ConnectionStrings:DefaultConnection %>' SelectCommand="SELECT [Id], [Name] FROM [AspNetRoles]"></asp:SqlDataSource>

    </form>
</body>
</html>
