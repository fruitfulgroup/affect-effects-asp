﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using Owin;
using BrightAffect.Models;
using System.Web.Security;

namespace BrightAffect.UserPages.Administration
{
    public partial class NewUserForm : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            success_div.Visible = false;
            alert_div.Visible = false;
        }

        protected void CreateNewAccount_Click(object sender, EventArgs e)
        {
            if (IsValid)
            {
                try
                {
                    success_div.Visible = false;
                    alert_div.Visible = false;

                    //Get client ID of current user so that newly created one have the same id.
                    string clientID = Request.QueryString["ClientID"];

                    //Try Create User account
                    var manager = Context.GetOwinContext().GetUserManager<ApplicationUserManager>();
                    var user = new ApplicationUser() { UserName = TextBox_Email.Text, Email = TextBox_Email.Text };
                    IdentityResult result = manager.Create(user, TextBox_Password.Text);
                    
                    if (result.Succeeded)
                    {

                        var newUser = manager.FindByName(TextBox_Email.Text);

                        SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString);
                        SqlCommand cmd = new SqlCommand();
                        cmd.Connection = conn;
                        cmd.CommandText = "INSERT INTO AspNetUserClients(ClientID, UserId) VALUES (@ClientId, @UserId)";
                        cmd.CommandType = CommandType.Text;
                        cmd.Parameters.AddWithValue("@UserId", newUser.Id);
                        cmd.Parameters.AddWithValue("@ClientID", clientID);
                        conn.Open();
                        cmd.ExecuteNonQuery();
                        conn.Close();

                        success_div.Visible = true;
                    }
                    else
                    {
                        alert_div.Visible = true;
                        alert_message.Text = result.Errors.FirstOrDefault();
                    }
                }
                catch (Exception ex)
                {
                    alert_div.Visible = true;
                    alert_message.Text = "Unknown Error: " + ex.ToString();
                }
            }
        }

    }

}