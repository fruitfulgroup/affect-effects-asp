﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Languages.aspx.cs" Inherits="BrightAffect.UserPages.Languages" %>

<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <title>Areas Form</title>
    <asp:PlaceHolder runat="server">
        <%: Scripts.Render("~/bundles/modernizr") %>
    </asp:PlaceHolder>
    <webopt:BundleReference runat="server" Path="~/Content/css" />
    <link href="~/favicon.ico" rel="shortcut icon" type="image/x-icon" />
    
    <!-- Web Fonts  -->
	<link href="http://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700,800|Shadows+Into+Light" rel="stylesheet" type="text/css"/>

	<!-- Affect Learning CSS -->
	<link rel="stylesheet" href="~/Content/font-awesome.css"/>
    <link rel="stylesheet" href="~/Content/Site.css"/>
    <link rel="stylesheet" href="~/Content/blue.css"/>
    <link rel="stylesheet" href="~/Content/css/theme-responsive.css" />

</head>
<body style="height:95%; width:95%">
    <form id="form1" runat="server" style="margin:10px;">
        <script type="text/javascript">

            function Save() {
                var grid = $find('<%=RadGrid_Languages.ClientID%>');
                var masterTable = grid.get_masterTableView();
                var batchManager = grid.get_batchEditingManager();
                var hasChanges = batchManager.hasChanges(masterTable);
                if (hasChanges) {
                    batchManager.saveAllChanges();
                }
            }

            function Cancel() {
                var grid = $find('<%=RadGrid_Languages.ClientID%>');
                grid.get_batchEditingManager().cancelChanges(grid.get_masterTableView());
            }

            function AddNew() {
                var grid = $find('<%=RadGrid_Languages.ClientID%>');
                grid.get_batchEditingManager().addNewRecord(grid.get_masterTableView());
            }

            function Refresh() {
                var masterTable = $find("<%= RadGrid_Languages.ClientID %>").get_masterTableView();
                masterTable.rebind();
            }

    </script>
        <telerik:RadScriptManager runat="server">
            <Scripts>
                <%--To learn more about bundling scripts in ScriptManager see http://go.microsoft.com/fwlink/?LinkID=301884 --%>
                <%--Framework Scripts--%>
                <asp:ScriptReference Name="MsAjaxBundle" />
                <asp:ScriptReference Name="jquery" />
                <asp:ScriptReference Name="bootstrap" />
                <asp:ScriptReference Name="respond" />
                <asp:ScriptReference Name="WebForms.js" Assembly="System.Web" Path="~/Scripts/WebForms/WebForms.js" />
                <asp:ScriptReference Name="WebUIValidation.js" Assembly="System.Web" Path="~/Scripts/WebForms/WebUIValidation.js" />
                <asp:ScriptReference Name="MenuStandards.js" Assembly="System.Web" Path="~/Scripts/WebForms/MenuStandards.js" />
                <asp:ScriptReference Name="GridView.js" Assembly="System.Web" Path="~/Scripts/WebForms/GridView.js" />
                <asp:ScriptReference Name="DetailsView.js" Assembly="System.Web" Path="~/Scripts/WebForms/DetailsView.js" />
                <asp:ScriptReference Name="TreeView.js" Assembly="System.Web" Path="~/Scripts/WebForms/TreeView.js" />
                <asp:ScriptReference Name="WebParts.js" Assembly="System.Web" Path="~/Scripts/WebForms/WebParts.js" />
                <asp:ScriptReference Name="Focus.js" Assembly="System.Web" Path="~/Scripts/WebForms/Focus.js" />
                <asp:ScriptReference Name="WebFormsBundle" />

                <%--Site Scripts--%>
                <asp:ScriptReference Assembly="Telerik.Web.UI" Name="Telerik.Web.UI.Common.Core.js"></asp:ScriptReference>
                <asp:ScriptReference Assembly="Telerik.Web.UI" Name="Telerik.Web.UI.Common.jQuery.js"></asp:ScriptReference>
                <asp:ScriptReference Assembly="Telerik.Web.UI" Name="Telerik.Web.UI.Common.jQueryInclude.js"></asp:ScriptReference>
            </Scripts>
        </telerik:RadScriptManager>
    
        <div class="row">
            <div class="col-sm-12">
                <h1><strong>Areas</strong></h1>
                <asp:Button ID="Button_NewItem" runat="server" Text="New Item" OnClientClick="AddNew(); return false;" CssClass="btn btn-primary btn-sm" />
                <asp:Button ID="Button_Save" runat="server" Text="Save Changes" OnClientClick="Save(); return false;" CssClass="btn btn-success btn-sm" />
                <asp:Button ID="Button_Cancel" runat="server" Text="Cancel Changes" OnClientClick="Cancel(); return false;"  CssClass="btn btn-danger btn-sm" />
                <asp:Button ID="Button_Refresh" runat="server" Text="Refresh" OnClientClick="Refresh(); return false;"  style="float:right;"  CssClass="btn btn-info btn-sm" />
                <hr />
            </div>
        </div>
        <div class="row">
            <div class="col-sm-12">
                <telerik:RadGrid ID="RadGrid_Languages" runat="server" CellSpacing="-1" DataSourceID="SqlDataSource_Languages"  GroupPanelPosition="Top" AllowAutomaticDeletes="True" AllowAutomaticInserts="True" AllowAutomaticUpdates="True" AutoGenerateColumns="False" PageSize="20">
                    <MasterTableView DataKeyNames="ID" DataSourceID="SqlDataSource_Languages" EditMode="Batch">
                        <Columns> 
                            <telerik:GridBoundColumn DataField="ID" HeaderText="ID" SortExpression="ID" UniqueName="ID" FilterControlAltText="Filter ID column" ReadOnly="true"/>                           
                            <telerik:GridBoundColumn DataField="Code" HeaderText="Code" SortExpression="Code" UniqueName="Code" FilterControlAltText="Filter Code column"/>
                            <telerik:GridBoundColumn DataField="Description" HeaderText="Description" SortExpression="Description" UniqueName="Description" FilterControlAltText="Filter Description column"/>
                            <telerik:GridBoundColumn DataField="Translation" HeaderText="Translation" SortExpression="Translation" UniqueName="Translation" FilterControlAltText="Filter Translation column"/>
                            <telerik:GridButtonColumn Text="Delete" CommandName="Delete" ButtonType="PushButton" UniqueName="DeleteColumn" ConfirmText="Are you sure you want to delete this Language?" ConfirmDialogType="Classic" ButtonCssClass="btn btn-danger btn-xs" HeaderStyle-Width="70px"/>
                        </Columns>
                    </MasterTableView>
                </telerik:RadGrid>
            </div>
        </div>

        <asp:SqlDataSource runat="server" ID="SqlDataSource_Languages" ConnectionString='<%$ ConnectionStrings:DefaultConnection %>'
            SelectCommand="SELECT * FROM Languages ORDER BY Code"
            DeleteCommand="DELETE FROM Languages WHERE (ID = @ID)"
            InsertCommand="INSERT INTO Languages(Code, Description, Translation) VALUES (@Code, @Description, @Translation)"
            UpdateCommand="UPDATE Languages SET Code = @Code, Description = @Description, Translation = @Translation WHERE (ID = @ID)">
            <DeleteParameters>
                <asp:Parameter Name="ID"></asp:Parameter>
            </DeleteParameters>
            <InsertParameters>
                <asp:Parameter Name="Code"></asp:Parameter>
                <asp:Parameter Name="Description"></asp:Parameter>
            </InsertParameters>
            <UpdateParameters>
                <asp:Parameter Name="Code"></asp:Parameter>
                <asp:Parameter Name="Description"></asp:Parameter>
                <asp:Parameter Name="ID"></asp:Parameter>
            </UpdateParameters>
        </asp:SqlDataSource>

    </form>
</body>
</html>