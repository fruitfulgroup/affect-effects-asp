﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Telerik.Web.UI;

namespace BrightAffect.UserPages
{
    public partial class Clients : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void Button_NewClient_Click(object sender, EventArgs e)
        {
            RadGrid_Clients.MasterTableView.IsItemInserted = true;
            RadGrid_Clients.Rebind();
        }

        protected void RadGrid_Clients_ItemCommand(object sender, Telerik.Web.UI.GridCommandEventArgs e)
        {
            if (e.CommandName == "View")
            {
                Response.Redirect("/UserPages/ClientDetails?ID=" + ((GridDataItem)e.Item).GetDataKeyValue("ID").ToString());
            }
        }
    }
}