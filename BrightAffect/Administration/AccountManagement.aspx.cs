﻿using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using Owin;
using BrightAffect.Models;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Telerik.Web.UI;

namespace BrightAffect.Account
{
    public partial class AccountManagement : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
           /* if (!Page.IsPostBack)
            {
                SqlDataSource_Users.SelectParameters["UserID"].DefaultValue = User.Identity.GetUserId().ToString();
            }
            */

            //On Page load hide messages.
            HideErrorMessage();
            HideSuccessMessage();
        }

       

#region User RadGrid Functions

        
        public void RadGridUsers_DeleteCommand(object source, Telerik.Web.UI.GridCommandEventArgs evtArgs)
        {
            //Note: ASP Identity does NOT have a method to delete users. So we do it manually.

            try
            {
                HideErrorMessage();
                HideSuccessMessage();
                User.Identity.GetUserId();
                
                //First get the User ID and Role
                string UserID = evtArgs.Item.OwnerTableView.DataKeyValues[evtArgs.Item.ItemIndex]["Id"].ToString();
                
               
                //Create SQL database connection. 
                //Select command get the Admins in the system. 
                //Delete command deletes the user from the Users and Roles Table.
                string connString = ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString;
                SqlDataSource sqlData = new SqlDataSource(connString, "");
                sqlData.SelectCommand = "SELECT * FROM [AspNetUsers]";
                sqlData.DeleteCommand = "DELETE FROM [AspNetUsers] WHERE [AspNetUsers].Id = @Id;";

                System.Data.DataView dv = (System.Data.DataView)sqlData.Select(DataSourceSelectArguments.Empty);
                
                if(dv.Count <= 2){
                    DisplayErrorMessage("You can not delete the last two accounts.");
                }
                else{
                    sqlData.DeleteParameters.Add(new Parameter("Id", System.Data.DbType.String, UserID));
                    sqlData.Delete();
                    DisplaySuccessMessage("User Account Deleted.");
                }
                
            }
            catch (Exception e) { DisplayErrorMessage(e.ToString()); }

        }

#endregion

        

#region Success/Error Message Displaying Functions

        protected void DisplaySuccessMessage(string message)
        {
            success_div.Visible = true;
            success_message.Text = message;
        }

        protected void HideSuccessMessage()
        {
            success_div.Visible = false;
            success_message.Text = "Unknown";
        }

        protected void DisplayErrorMessage(string message)
        {
            alert_div.Visible = true;
            alert_message.Text = message;
        }

        protected void HideErrorMessage()
        {
            alert_div.Visible = false;
            alert_message.Text = "Unknown";
        }

#endregion


    }
}