﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace BrightAffect.API
{
    public partial class IsTokenValid : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            string User = Request.Form["Username"];
            string Token = Request.Form["Token"];

            if (FruitfulAPI.IsTokenValid(User, Token)) Response.Write("Yes");
            else Response.Write("No");
        }
    }
}