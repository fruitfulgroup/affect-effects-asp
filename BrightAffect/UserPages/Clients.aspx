﻿<%@ Page Title="Clients" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Clients.aspx.cs" Inherits="BrightAffect.UserPages.Clients" %>

<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>

<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
    <div class="row">
        <div class="col-sm-6">
            <h1><strong>Clients</strong></h1>
        </div>
        <div class="col-sm-6">
            <br />
            <asp:Button ID="Button_NewClient" runat="server" Text="Create New Client" style="float:right" OnClick="Button_NewClient_Click" CssClass="btn btn-primary" />
        </div>
        <div class="col-sm-12"><hr /></div>
    </div>
    <div class="row">
        <div class="col-sm-12">
            <telerik:RadGrid ID="RadGrid_Clients" runat="server" CellSpacing="-1" DataSourceID="SqlDataSource_Clients" GroupPanelPosition="Top" AllowAutomaticDeletes="True" AllowAutomaticInserts="True" AllowAutomaticUpdates="True" AllowFilteringByColumn="True" AllowPaging="True" AllowSorting="True" PageSize="20" OnItemCommand="RadGrid_Clients_ItemCommand" Skin="Default">
                <MasterTableView DataSourceID="SqlDataSource_Clients" DataKeyNames="ID" AutoGenerateColumns="False">
                    <Columns>
                        <telerik:GridBoundColumn DataField="ID" HeaderText="Client ID" SortExpression="ID" UniqueName="ID" FilterControlAltText="Filter ID column" HeaderStyle-Width="1%"/>
                        <telerik:GridBoundColumn DataField="Name" HeaderText="Client Name" SortExpression="Name" UniqueName="Name" FilterControlAltText="Filter Name column"/>                        
                        <telerik:GridBoundColumn DataField="NumVideos" HeaderText="No. Videos" SortExpression="NumVideos" UniqueName="NumVideos" FilterControlAltText="Filter NumVideos column" ReadOnly="true"/>
                        <telerik:GridBoundColumn DataField="Languages" HeaderText="Languages" SortExpression="Languages" UniqueName="Languages" FilterControlAltText="Filter Languages column" ReadOnly="true"/>
                        <telerik:GridButtonColumn CommandName="View" Text="View Client" UniqueName="ViewButton" ButtonType="PushButton" ButtonCssClass="btn btn-xs btn-primary" HeaderStyle-Width="1%" />
                        <telerik:GridButtonColumn CommandName="Edit" Text="Edit" UniqueName="EditButton" ButtonType="PushButton" ButtonCssClass="btn btn-xs btn-success" HeaderStyle-Width="1%" />
                        <telerik:GridButtonColumn CommandName="Delete" Text="Delete" UniqueName="DeleteButton" ConfirmText="Are you sure you want to delete this Client?" ConfirmDialogType="Classic" ButtonType="PushButton" ButtonCssClass="btn btn-xs btn-danger" HeaderStyle-Width="1%" />
                    </Columns>
                </MasterTableView>
            </telerik:RadGrid>
        </div>
    </div>

    <asp:SqlDataSource runat="server" ID="SqlDataSource_Clients" ConnectionString='<%$ ConnectionStrings:DefaultConnection %>'
        SelectCommand="SELECT * FROM vw_Clients ORDER BY Name" 
        DeleteCommand="DELETE FROM Clients WHERE (ID = @ID)" 
        InsertCommand="INSERT INTO Clients(Name) VALUES (@Name)" 
        UpdateCommand="UPDATE Clients SET Name = @Name WHERE (ID = @ID)">
        <DeleteParameters>
            <asp:Parameter Name="ID"></asp:Parameter>
        </DeleteParameters>
        <InsertParameters>
            <asp:Parameter Name="Name"></asp:Parameter>
        </InsertParameters>
        <UpdateParameters>
            <asp:Parameter Name="Name"></asp:Parameter>
            <asp:Parameter Name="ID"></asp:Parameter>
        </UpdateParameters>
    </asp:SqlDataSource>

    <telerik:RadAjaxLoadingPanel ID="RadAjaxLoadingPanel" runat="server" Skin="Default"></telerik:RadAjaxLoadingPanel>

    <telerik:RadAjaxManager ID="RadAjaxManager1" runat="server">
        <AjaxSettings>
            <telerik:AjaxSetting AjaxControlID="RadGrid_Clients"><UpdatedControls><telerik:AjaxUpdatedControl ControlID="RadGrid_Clients" LoadingPanelID="RadAjaxLoadingPanel1"/></UpdatedControls></telerik:AjaxSetting>
            <telerik:AjaxSetting AjaxControlID="Button_NewClient"><UpdatedControls><telerik:AjaxUpdatedControl ControlID="RadGrid_Clients" LoadingPanelID="RadAjaxLoadingPanel1"/></UpdatedControls></telerik:AjaxSetting>
        </AjaxSettings>
    </telerik:RadAjaxManager>

</asp:Content>
