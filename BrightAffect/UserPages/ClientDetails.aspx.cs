﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Telerik.Web.UI;

namespace BrightAffect.UserPages
{
    public partial class ClientDetails : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void Button_NewCourse_Click(object sender, EventArgs e)
        {
            RadGrid_Courses.MasterTableView.IsItemInserted = true;
            RadGrid_Courses.Rebind();
        }

        protected void Button_CourseTitle_Click(object sender, EventArgs e)
        {
            RadGrid_CourseTitles.MasterTableView.IsItemInserted = true;
            RadGrid_CourseTitles.Rebind();
        }


        protected void Button_AddCourseVideo_Click(object sender, EventArgs e)
        {
            RadGrid_CourseVideos.MasterTableView.IsItemInserted = true;
            RadGrid_CourseVideos.Rebind();
        }

        protected void ButtonNewVideo_Click(object sender, EventArgs e)
        {
            RadGrid_Videos.MasterTableView.IsItemInserted = true;
            RadGrid_Videos.Rebind();
        }

        protected void Button_NewVideoItem_Click(object sender, EventArgs e)
        {
            RadGrid_VideoItems.MasterTableView.IsItemInserted = true;
            RadGrid_VideoItems.Rebind();
        }

        protected void Button_NewLanguage_Click(object sender, EventArgs e)
        {
            RadGrid_Languages.MasterTableView.IsItemInserted = true;
            RadGrid_Languages.Rebind();
        }

        protected void RadGrid_Courses_ItemInserted(object sender, Telerik.Web.UI.GridInsertedEventArgs e)
        {
            SqlConnection conn = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString);
            SqlCommand cmd0 = new SqlCommand();
            SqlCommand cmd1 = new SqlCommand();
            cmd0.Connection = conn;
            cmd1.Connection = conn;
            String loc = "";


            cmd1.CommandText = "SELECT MAX([ID]) FROM [CourseTranslations] WHERE ClientID = @ClientID";
            cmd1.Parameters.AddWithValue("@ClientID", Request.QueryString["ID"]);
            conn.Open();
            int newID = Int32.Parse(cmd1.ExecuteScalar().ToString());
            conn.Close();
            
            GridEditFormItem item = (GridEditFormItem)e.Item;
            RadAsyncUpload uploader = (RadAsyncUpload)item.FindControl("AsyncUpload");
            if (uploader.UploadedFiles.Count > 0)
            {
                const string relativePath = "~/Images/";
                UploadedFile file = uploader.UploadedFiles[0];
                var filename = file.FileName;  //name of the file
                string str = Guid.NewGuid().ToString() + filename;
                var physicalSavePath = MapPath(relativePath) + str;
                loc = relativePath + str;
                file.SaveAs(physicalSavePath, true);
            }

            cmd0.CommandText = "UPDATE [CourseTranslations] SET PhotoLink = @ImgLoc WHERE ID = @ID";
            cmd0.Parameters.AddWithValue("@ImgLoc", loc);
            cmd0.Parameters.AddWithValue("@ID", newID);
            cmd0.CommandType = System.Data.CommandType.Text;
            conn.Open();
            cmd0.ExecuteNonQuery();
            conn.Close();
        }

        protected void RadGrid_Courses_ItemUpdated(object sender, Telerik.Web.UI.GridUpdatedEventArgs e)
        {
            RadAsyncUpload uploader = (RadAsyncUpload)e.Item.FindControl("AsyncUpload");
            if (uploader.UploadedFiles.Count > 0)
            {
                const string relativePath = "~/Images/";
                UploadedFile file = uploader.UploadedFiles[0];
                var filename = file.FileName;  //name of the file
                string str = Guid.NewGuid().ToString() + filename;
                var physicalSavePath = MapPath(relativePath) + str;
                String loc = relativePath + str;
                file.SaveAs(physicalSavePath, true);


                try { System.IO.File.Delete(Server.MapPath(e.Item.GetDataKeyValue("PhotoLink").ToString())); }
                catch { }

                SqlConnection conn = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString);
                SqlCommand cmd = new SqlCommand();
                cmd.Connection = conn;
                cmd.CommandText = "UPDATE [CourseTranslations] SET PhotoLink = @ImgLoc WHERE ID = @ID";
                cmd.Parameters.AddWithValue("@ImgLoc", loc);
                cmd.Parameters.AddWithValue("@ID", e.Item.GetDataKeyValue("ID"));

                cmd.CommandType = System.Data.CommandType.Text;
                conn.Open();
                cmd.ExecuteNonQuery();
                conn.Close();
            }
        }


        protected void RadGrid_Courses_ItemDeleted(object sender, GridDeletedEventArgs e)
        {
            try { System.IO.File.Delete(Server.MapPath(e.Item.GetDataKeyValue("PhotoLink").ToString())); }
            catch { }            
        }

        protected void RadGrid_CourseVideos_ItemCommand(object sender, GridCommandEventArgs e)
        {
            if (e.CommandName == "MoveUp" || e.CommandName == "MoveDown")
            {
                GridDataItem item = (GridDataItem)e.Item;
                SqlDataSource_CourseVideosMove.SelectParameters["ID1"].DefaultValue = item.GetDataKeyValue("ID").ToString();
                SqlDataSource_CourseVideosMove.SelectParameters["Pos1"].DefaultValue = item.GetDataKeyValue("Position").ToString();
                SqlDataSource_CourseVideosMove.SelectParameters["CourseID"].DefaultValue = item.GetDataKeyValue("CourseID").ToString();
                SqlDataSource_CourseVideosMove.SelectParameters["ClientID"].DefaultValue = item.GetDataKeyValue("ClientID").ToString();

                if (e.CommandName == "MoveUp")
                    SqlDataSource_CourseVideosMove.SelectParameters["MoveBy"].DefaultValue = "-1";
                else if (e.CommandName == "MoveDown")
                    SqlDataSource_CourseVideosMove.SelectParameters["MoveBy"].DefaultValue = "1";


                SqlDataSource_CourseVideosMove.Select(DataSourceSelectArguments.Empty);
                RadGrid_CourseVideos.Rebind();
            }
        }

        protected void RadGrid_VideoList_Add(object sender, GridCommandEventArgs e)
        {
            RadGrid_CourseVideos.Rebind();
        }

        protected void RadGrid_VideoItems_ItemCreated(object sender, GridItemEventArgs e)
        {
            try
            {
                if (e.Item is GridEditFormInsertItem)
                {
                    GridEditFormInsertItem editform = (GridEditFormInsertItem)e.Item;
                    RadDatePicker datepcker = (RadDatePicker)editform.FindControl("RadDatePicker");
                    datepcker.SelectedDate = DateTime.Now;
                }
            }
            catch { }
        }

      
        
    }
}