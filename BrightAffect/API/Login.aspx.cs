﻿using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using Owin;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Configuration;
using System.Data;
using BrightAffect.Models;


namespace BrightAffect.API
{
    public partial class Login : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                string UserName = Request.Form["Username"];
                string Password = Request.Form["Password"];

                //Prevent SQL injection here
                if (UserName == null) { Response.Write(string.Format("{{ \"error\" : \"Username not input\" }}")); return; }
                if (Password == null) { Response.Write(string.Format("{{ \"error\" : \"Password not input\" }}")); return; }


                // Validate the user password
                var manager = Context.GetOwinContext().GetUserManager<ApplicationUserManager>();
                
                ApplicationUser user = manager.Find(UserName, Password);
                        
                if (user != null)
                {
                    string connString = ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString;
                    SqlDataSource sqlDataSource = new SqlDataSource(connString, "");
                    sqlDataSource.SelectCommand = "SELECT ClientID FROM [AspNetUserClients] WHERE UserId = @UserId";
                    sqlDataSource.InsertCommand = "INSERT INTO [AspNetUserTokens]([UserId], [Token], [Created]) VALUES (@UserId, @Token, GETDATE())";
                    sqlDataSource.DeleteCommand = "DELETE [AspNetUserTokens] WHERE Created < DATEADD(month, -3, GETDATE()) ";

                    //Select ClientID of User
                    sqlDataSource.SelectParameters.Add(new Parameter("UserId", DbType.String, user.Id));
                    DataTable data = (sqlDataSource.Select(DataSourceSelectArguments.Empty) as DataView).ToTable();

                    string clientId = data.Rows[0][0].ToString();

                    //Delete Old Token(s)
                    sqlDataSource.DeleteParameters.Add(new Parameter("UserId", DbType.String, user.Id));
                    sqlDataSource.Delete();

                    
                    //Insert New Token
                    string NewToken = "" + System.Guid.NewGuid().ToString("N");

                    sqlDataSource.InsertParameters.Add(new Parameter("UserId", DbType.String, user.Id));
                    sqlDataSource.InsertParameters.Add(new Parameter("Token", DbType.String, NewToken));
                    sqlDataSource.Insert();

                    Response.Write(string.Format("{{ \"user\" : \"{0}\",\n \"token\" : \"{1}\", \n \"clientid\" : \"{2}\" }}", user.UserName, NewToken, clientId));
                    
                }
                else
                {
                    Response.Write(string.Format("{{ \"error\" : \"Invalid username or password\" }}"));
                }

                
            }

            catch(Exception ex)
            {
                Response.Write(string.Format("{{ \"error\" : \"Invalid username or password\" }}"));
            }


            

        }
    }
}