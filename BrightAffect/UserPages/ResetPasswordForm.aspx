﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="ResetPasswordForm.aspx.cs" Inherits="BrightAffect.Account.ResetPasswordForm" %>

<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <title>Reset User Password</title>
    <asp:PlaceHolder runat="server">
        <%: Scripts.Render("~/bundles/modernizr") %>
    </asp:PlaceHolder>
    <webopt:BundleReference runat="server" Path="~/Content/css" />
    <link href="~/favicon.ico" rel="shortcut icon" type="image/x-icon" />
</head>
<body>
    <form runat="server">
     <asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>
    
    <asp:UpdatePanel ID="UpdatePanel" runat="server">
        <ContentTemplate>

    <div style="padding: 30px; height: 400px; width:350px; overflow:no-display" >
        <%--Error Alert Message <DIV>. Used to display an error message. Hidden when created.--%>
        <div id="alert_div" class="alert alert-danger" runat="server" role="alert">
            <asp:Label runat="server" Text="Error!" Font-Bold="True"></asp:Label>
            <asp:Label ID="alert_message" runat="server" Text="Failed to create new user"></asp:Label>
        </div>
        <%--Success Alert Message <DIV>. Used to display a success message. Hidden when created.--%>
        <div id="success_div" class="alert alert-success" runat="server" role="alert">
            <asp:Label runat="server" Text="Sucess!" Font-Bold="True"></asp:Label>
            <asp:Label ID="success_message" runat="server" Text="Password Reset!"></asp:Label>
        </div>

        <div class="row">
            <div class="col-md-12">
                <section id="passwordForm">
                    <asp:PlaceHolder runat="server" ID="setPassword">

                        <div class="form-horizontal">
                            <h4>Reset Password Form</h4>
                            <hr />
                            
                            <div class="form-group">
                                <asp:Label ID="Label3" runat="server" AssociatedControlID="TextBox_Username" CssClass="col-md-2 control-label">Username</asp:Label>
                                <div class="col-md-10">
                                    <asp:TextBox runat="server" ID="TextBox_Username" CssClass="form-control" ReadOnly="true"/>
                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator6" runat="server" ControlToValidate="TextBox_Username"
                                        CssClass="text-danger" ErrorMessage="The username field is required."
                                        Display="Dynamic" ValidationGroup="SetPassword" />

                                </div>
                            </div>

                            <div class="form-group">
                                <asp:Label ID="Label1" runat="server" AssociatedControlID="TextBox_Password" CssClass="col-md-2 control-label">Password</asp:Label>
                                <div class="col-md-10">
                                    <asp:TextBox runat="server" ID="TextBox_Password" TextMode="Password" CssClass="form-control" />
                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="TextBox_Password"
                                        CssClass="text-danger" ErrorMessage="The password field is required."
                                        Display="Dynamic" ValidationGroup="SetPassword" />
                                    <asp:ModelErrorMessage ID="ModelErrorMessage1" runat="server" ModelStateKey="NewPassword" AssociatedControlID="TextBox_Password"
                                        CssClass="text-danger" SetFocusOnError="true" />
                                </div>
                            </div>

                            <div class="form-group">
                                <asp:Label ID="Label2" runat="server" AssociatedControlID="TextBox_ConfirmPassword" CssClass="col-md-2 control-label">Confirm password</asp:Label>
                                <div class="col-md-10">
                                    <asp:TextBox runat="server" ID="TextBox_ConfirmPassword" TextMode="Password" CssClass="form-control" />
                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ControlToValidate="TextBox_ConfirmPassword"
                                        CssClass="text-danger" Display="Dynamic" ErrorMessage="The confirm password field is required."
                                        ValidationGroup="SetPassword" />
                                    <asp:CompareValidator ID="CompareValidator1" runat="server" ControlToCompare="TextBox_Password" ControlToValidate="TextBox_ConfirmPassword"
                                        CssClass="text-danger" Display="Dynamic" ErrorMessage="The password and confirmation password do not match."
                                        ValidationGroup="SetPassword" />
                                </div>
                            </div>
                            <hr />
                            <div class="form-group">
                                <div class="col-md-12">
                                    <asp:Button ID="Button1" runat="server" Class="btn btn-info" Text="Reset Password" ValidationGroup="SetPassword" OnClick="ResetPassword_Click" Style="width:100%" />
                                </div>
                            </div>
                        </div>
                    </asp:PlaceHolder>

                </section>
            </div>
        </div>
        </div>

            </ContentTemplate>
        </asp:UpdatePanel>

    </form>
</body>
</html>
