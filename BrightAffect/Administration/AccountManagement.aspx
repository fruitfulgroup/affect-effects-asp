﻿<%@ Page Title="User Account Management" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="AccountManagement.aspx.cs" Inherits="BrightAffect.Account.AccountManagement" %>


<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>


<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">

    <%--Error Telerik Rad Window Manager. Used to display Popup Dialogs and the Reset Password Window--%>
    <telerik:RadWindowManager ID="RadWindowManager1" Skin="Simple" runat="server">
        <Windows>
            <%--Here is the reset password window--%>
            <telerik:RadWindow ID="RadWindow1" runat="server" ShowContentDuringLoad="false" Width="400px"
                Height="400px" Title="" Behaviors="Move,Close" OnClientClose="RefreshGrid">
            </telerik:RadWindow>
        </Windows>
    </telerik:RadWindowManager>

    <telerik:RadCodeBlock ID="RadCodeBlock1" runat="server">
        <script type="text/javascript">
            //This Javasript function opens the Reset Password Page in a Popout window.
            //The Username is passed as a request parameter.
            function OpenRestWindow(UserName) {
                var url = "/Administration/ResetPassword?User=" + UserName;
                radopen(url, "RadWindow1", 400, 550, 100, 100);
            }

            function OpenNewUserWindow() {
                var url = "/Administration/NewUser";
                radopen(url, "RadWindow1", 400, 650, 100, 100);
            }

            function OpenEditRolesWindow(UserName, Id) {
                var url = "/Administration/EditUserRoles?User=" + UserName + "&UserId=" + Id;
                radopen(url, "RadWindow1", 400, 550, 100, 100);
            }

            function RefreshGrid() {
                var masterTable = $find("<%=RadGrid_Users.ClientID%>").get_masterTableView();
                masterTable.rebind();
            }

            function RaiseCommand(sender, eventArgs) {
                var radgrid = $find('<%= RadGrid_Users.ClientID %>');
                //var tableView = radgrid.get_masterTableView();
                var items = radgrid.get_masterTableView().get_dataItems();

                var id = items[eventArgs.get_commandArgument()].getDataKeyValue('Id');
                var user = items[eventArgs.get_commandArgument()].getDataKeyValue('UserName');

                if (eventArgs.get_commandName() == 'EditRoles') {
                    OpenEditRolesWindow(user, id);
                }
                else if (eventArgs.get_commandName() == 'Reset') {
                    OpenRestWindow(user);
                }
            }

         </script>
    </telerik:RadCodeBlock>

    

    
            <%--Error Alert Message <DIV>. Used to display an error message. Hidden On PageLoad.--%>
            <div id="alert_div" class="alert alert-danger" runat="server" role="alert">
                <asp:Label runat="server" Text="Error!" Font-Bold="True"></asp:Label>
                <asp:Label ID="alert_message" runat="server" Text="Label"></asp:Label>
            </div>
            <%--Success Alert Message <DIV>. Used to display a success message. Hidden On PageLoad.--%>
            <div id="success_div" class="alert alert-success" runat="server" role="alert">
                <asp:Label runat="server" Text="Sucess!" Font-Bold="True"></asp:Label>
                <asp:Label ID="success_message" runat="server" Text="Label"></asp:Label>
            </div>

            <div class="row">                
                <div class="col-xs-6" >
                    <h2><strong>User Account Management</strong></h2>
                </div>
                <div class="col-xs-6">
                    <br />
                    <asp:Button ID="Button_Refresh" CssClass="btn btn-success" runat="server" Text="Refresh" style="float:right; margin-left:5px;" OnClientClick="RefreshGrid()" />
                    <asp:Button ID="Button_NewUser" CssClass="btn btn-primary" runat="server" Text="Create New User" style="float:right;" OnClientClick="OpenNewUserWindow()" />
                </div>

            </div>
           

            <%-- Rad Grid for User Accounts --%>
            <div class="row">
                <div class="col-xs-12">
                    <hr />
                    <telerik:RadGrid ID="RadGrid_Users" runat="server" DataSourceID="SqlDataSource_Users" AllowPaging="True" AutoGenerateColumns="False" CellSpacing="-1" 
                        OnDeleteCommand="RadGridUsers_DeleteCommand" PageSize="20" GroupPanelPosition="Top" Skin="Default">
                        <GroupingSettings CaseSensitive="false" />
                        <MasterTableView DataKeyNames="Id,UserName" ClientDataKeyNames="Id,UserName" DataSourceID="SqlDataSource_Users">
                            <Columns>
                                <telerik:GridBoundColumn DataField="UserName" HeaderText="Username" SortExpression="UserName" UniqueName="UserName" FilterControlAltText="Filter UserName column" ItemStyle-Width="10%" />
                                <telerik:GridBoundColumn DataField="Roles" HeaderText="Roles" SortExpression="Roles" UniqueName="Roles" FilterControlAltText="Filter Roles column" ItemStyle-Width="30%" />
                                <%--<telerik:GridButtonColumn CommandName="EditRoles" Text="Edit Roles" UniqueName="EditRolesButton" ButtonType="PushButton" ButtonCssClass="btn btn-primary btn-xs" HeaderStyle-Width="1%" />--%>
                                <telerik:GridButtonColumn CommandName="Reset" Text="Reset Password" UniqueName="ResetPasswordButton" ButtonType="PushButton" ButtonCssClass="btn btn-warning btn-xs" HeaderStyle-Width="1%" />
                                <telerik:GridButtonColumn CommandName="Delete" Text="Delete Account" UniqueName="DeleteButton" ConfirmText="Are you sure you want to delete this Account?" ConfirmDialogType="Classic" Reorderable="False" ButtonType="PushButton" ButtonCssClass="btn btn-danger btn-xs" HeaderStyle-Width="1%" />
                            </Columns>
                        </MasterTableView>
                        <ClientSettings>
                            <ClientEvents OnCommand="RaiseCommand" />
                        </ClientSettings>
                    </telerik:RadGrid>
                </div>
            </div>


    <%-- SQL Datasource for the Users --%>
    <%-- Notes: 
        - Select gets the list of Users. Only
    --%>
    <asp:SqlDataSource ID="SqlDataSource_Users" runat="server" ConnectionString='<%$ ConnectionStrings:DefaultConnection %>'
        SelectCommand="SELECT Users.Id, Users.UserName, STUFF( (SELECT ', ' + Name  FROM AspNetRoles INNER JOIN AspNetUserRoles ON AspNetRoles.Id = RoleId WHERE UserId = Users.Id ORDER BY Name   FOR XML PATH('')),   1, 1, '') AS Roles FROM AspNetUsers Users  LEFT OUTER JOIN AspNetUserRoles ON Users.Id = RoleId INNER JOIN AspNetUserAdmins Admins ON Users.Id = Admins.UserID">
        <SelectParameters>
            
        </SelectParameters>
    </asp:SqlDataSource>


    <telerik:RadAjaxManager ID="RadAjaxManager1" runat="server">
        <AjaxSettings>
            <telerik:AjaxSetting AjaxControlID="Button_NewUser"> <UpdatedControls> <telerik:AjaxUpdatedControl ControlID="RadGrid_Users" LoadingPanelID="RadAjaxLoadingPanel1"/></UpdatedControls> </telerik:AjaxSetting>
            <telerik:AjaxSetting AjaxControlID="Button_Refresh"> <UpdatedControls> <telerik:AjaxUpdatedControl ControlID="RadGrid_Users" LoadingPanelID="RadAjaxLoadingPanel1"/> </UpdatedControls> </telerik:AjaxSetting>
            <telerik:AjaxSetting AjaxControlID="RadGrid_Users"> <UpdatedControls> <telerik:AjaxUpdatedControl ControlID="RadGrid_Users" LoadingPanelID="RadAjaxLoadingPanel1"/> </UpdatedControls> </telerik:AjaxSetting>
        </AjaxSettings>
    </telerik:RadAjaxManager>

    
</asp:Content>

