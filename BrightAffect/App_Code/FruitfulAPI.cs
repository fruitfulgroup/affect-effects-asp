﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.Script.Serialization;

    public static class FruitfulAPI
    {

        public static bool IsTokenValid(string UserName, string Token)
        {

            //Make Connection to Database
            SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString);
            SqlDataAdapter adpt = new SqlDataAdapter();
            SqlCommand cmd = new SqlCommand();

            DataTable dt = new DataTable();
            conn.Open();

            try
            {
                cmd = new SqlCommand("SELECT Token FROM [AspNetUserTokens] INNER JOIN [AspNetUsers] ON [AspNetUserTokens].UserId = [AspNetUsers].Id WHERE [UserName] = @UserName AND [Token] = @Token", conn);
                cmd.Parameters.Add(new SqlParameter("@UserName", UserName));
                cmd.Parameters.Add(new SqlParameter("@Token", Token));
                cmd.CommandType = CommandType.Text;
                adpt.SelectCommand = cmd;
                adpt.Fill(dt);
            }
            catch { return false; }

            cmd.Dispose();
            conn.Close();

            if (dt.Rows.Count > 0)  return true;
            else return false;
        }
        
        public static string[] UploadFiles(HttpFileCollection fileCollection, HttpServerUtility server, string folderPath)
        {
            List<String> fileUrls = new List<string>();

            try
            {
                foreach (HttpPostedFile file in fileCollection)
                {
                    //Get an entry for file upload
                    string fileName = String.Format("{0}", System.Guid.NewGuid().ToString("N"));
                    String[] names = file.FileName.Split('.');
                    string src = fileName + names[names.Length - 2] + "." + names[names.Length - 1];

                    file.SaveAs(server.MapPath(folderPath) + src);
                    String strPathAndQuery = HttpContext.Current.Request.Url.PathAndQuery;
                    String strUrl = HttpContext.Current.Request.Url.AbsoluteUri.Replace(strPathAndQuery, "/");
                    fileUrls.Add("" + strUrl + folderPath.Replace("~/", "") + src);
                }   
            }
            catch (Exception)
            {
                
            }

            return fileUrls.ToArray();
        }


        public static string DataTableToJSONArray(DataTable table)
        {
            var list = new List<Dictionary<string, object>>();

            foreach (DataRow row in table.Rows)
            {
                var dict = new Dictionary<string, object>();

                foreach (DataColumn col in table.Columns)
                {
                    dict[col.ColumnName] = row[col];
                }
                list.Add(dict);
            }
            JavaScriptSerializer serializer = new JavaScriptSerializer();
            return serializer.Serialize(list);
        }



        public static string DataTableToJSON(DataTable table)
        {
            var list = new List<Dictionary<string, object>>();
            DataRow row = table.Rows[0];
            var dict = new Dictionary<string, object>();

            foreach (DataColumn col in table.Columns)
            {
                dict[col.ColumnName] = row[col];
            }

            JavaScriptSerializer serializer = new JavaScriptSerializer();
            return serializer.Serialize(dict);
        }


        public static string Sanitize(string stringValue)
        {
            if (null == stringValue)
                return stringValue;

            stringValue = RegexReplace(stringValue, "-{2,}", "-");           // transforms multiple --- in - use to comment in sql scripts
            stringValue = RegexReplace(stringValue, "[*/]+", string.Empty);      // removes / and * used also to comment in sql scripts  
            stringValue = RegexReplace(stringValue, "(;|exec|execute|select|insert|update|delete|create|alter|drop|rename|truncate|backup|restore)", string.Empty, RegexOptions.IgnoreCase);

            return stringValue;
        }

        private static string RegexReplace(string stringValue, string matchPattern, string toReplaceWith)
        {
            return Regex.Replace(stringValue, matchPattern, toReplaceWith);
        }

        private static string RegexReplace(string stringValue, string matchPattern, string toReplaceWith, RegexOptions regexOptions)
        {
            return Regex.Replace(stringValue, matchPattern, toReplaceWith, regexOptions);
        } 


    }
