﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;

namespace BrightAffect.API
{
    public partial class GetLanguages : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                //Make Connection to Database
                SqlConnection conn = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString);
                SqlDataAdapter adpt = new SqlDataAdapter();
                SqlCommand cmd = new SqlCommand("GetClientLanguages", conn);

                DataTable dt = new DataTable();

                conn.Open();

                cmd.Parameters.Add(new SqlParameter("@ClientID", Request.Form["ClientID"]));
               
                cmd.CommandType = CommandType.StoredProcedure;
                adpt.SelectCommand = cmd;
                adpt.Fill(dt);
                Response.Write(FruitfulAPI.DataTableToJSONArray(dt));
                cmd.Dispose();
                conn.Close();
            }
            catch { Response.Write(string.Format("{{ \"error\" : \"Database execution error.\" }}")); }
        }
    }
}