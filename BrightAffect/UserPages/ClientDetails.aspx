﻿<%@ Page Title="Client Details" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="ClientDetails.aspx.cs" Inherits="BrightAffect.UserPages.ClientDetails" %>

<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>


<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
        
    <div class="row">
        <div class="col-sm-12">
            <h1>Client Details</h1>
            <asp:FormView ID="FormView" runat="server" DataSourceID="SqlDataSource_ClientInfo">
                <ItemTemplate>                   
                    <h4><strong>Name:  <asp:Label Text='<%# Eval("Name") %>' runat="server" ID="NameLabel" /></strong></h4>
                </ItemTemplate>
            </asp:FormView>
            <hr />
        </div>
        <div class="col-sm-12">
            <telerik:RadTabStrip ID="RadTabStrip" runat="server" SelectedIndex="0" MultiPageID="RadMultiPage1" Width="100%" Skin="MetroTouch">
                <Tabs>
                    <telerik:RadTab runat="server" Text="Courses" PageViewID="CoursesPage"/>
                    <telerik:RadTab runat="server" Text="Videos" PageViewID="VideosPage"/>
                    <telerik:RadTab runat="server" Text="Languages" PageViewID="LanguagesPage"/>
                    <telerik:RadTab runat="server" Text="Accounts" PageViewID="AccountsPage"/>                    
                </Tabs>
            </telerik:RadTabStrip>
        </div>

        <div class="col-sm-12">
            <div style="border:solid; border-width:thin; border-color:gray;padding-top:-3px; min-height:400px;">
                <div class="panel-body">
                    <telerik:RadMultiPage ID="RadMultiPage1" runat="server" SelectedIndex="0" Width="100%">
                        <%-- Details for Courses Tab --%>
                        <telerik:RadPageView ID="CoursesPage" runat="server" Width="100%">
                            <div class="row">
                                <div class="col-sm-6">
                                    <h3>Courses</h3>
                                </div>
                                <div class="col-sm-6">
                                    <br />
                                    <asp:Button ID="Button_NewCourse" runat="server" Text="Add New Course" style="float:right;" OnClick="Button_NewCourse_Click" CssClass="btn btn-primary" />
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-sm-12">
                                    <telerik:RadGrid ID="RadGrid_Courses" runat="server" DataSourceID="SqlDataSource_Courses" GroupPanelPosition="Top" AllowAutomaticDeletes="True" AllowAutomaticInserts="True" AllowAutomaticUpdates="True" 
                                        AllowPaging="True" AutoGenerateColumns="False"  Skin="Default">
                                        <ClientSettings EnablePostBackOnRowClick="true">
                                            <Selecting AllowRowSelect="True"></Selecting>
                                        </ClientSettings>
                                        <MasterTableView DataSourceID="SqlDataSource_Courses" DataKeyNames="ID,ClientID" NoMasterRecordsText="No Course Headers Found" >
                                            <Columns>                                                
                                                <telerik:GridBoundColumn DataField="Title" HeaderText="Title" SortExpression="Title" UniqueName="Title" FilterControlAltText="Filter Title column"/>
                                                <telerik:GridButtonColumn CommandName="Edit" Text="Edit" UniqueName="EditButton" ButtonType="PushButton" ButtonCssClass="btn btn-success btn-sm"  HeaderStyle-Width="1%" />
                                                <telerik:GridButtonColumn CommandName="Delete" Text="Delete" UniqueName="DeleteButton" ConfirmText="Are you sure you want to delete this Course?" ConfirmDialogType="Classic" ButtonType="PushButton" ButtonCssClass="btn btn-danger btn-sm"  HeaderStyle-Width="1%" />
                                            </Columns>
                                            <EditFormSettings EditFormType="Template">
                                                <FormTemplate>
                                                    <table cellpadding="3px">                                                        
                                                        <tr>
                                                            <td><label>Title: </label></td>
                                                            <td><telerik:RadTextBox ID="TextBox1" runat="server" Text='<%# Bind( "Title") %>' CssClass="form-control"/>
                                                            </td>
                                                        </tr>                                                         
                                                     </table>
                                                    <hr />
                                                    <asp:Button ID="Button_Update" runat="server" style="margin:5px;" CssClass="btn btn-success btn-sm" Text='<%# (Container is GridEditFormInsertItem) ? "Insert" : "Update" %>' CommandName='<%# (Container is GridEditFormInsertItem) ? "PerformInsert" : "Update" %>' />
                                                    <asp:Button ID="Button_Cancel" runat="server" style="margin:5px;" CssClass="btn btn-danger btn-sm" Text="Cancel" CommandName="Cancel" CausesValidation="false" />
                                                </FormTemplate>
                                             </EditFormSettings>
                                        </MasterTableView>
                                    </telerik:RadGrid>
                                </div>
                            </div>
                            <br />
                            <hr />
                            <div class="col-sm-6">     
                                <div class="row">
                                    <div class="col-sm-6">
                                        <h3>Translations</h3>
                                    </div>
                                    <div class="col-sm-6">
                                        <br />
                                        <asp:Button ID="Button_NewCourseTitle" runat="server" Text="Add Title Translation" style="float:right;" OnClick="Button_CourseTitle_Click" CssClass="btn btn-primary" />
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-sm-12">
                                        <telerik:RadGrid ID="RadGrid_CourseTitles" runat="server" DataSourceID="SqlDataSource_CourseTranslations" GroupPanelPosition="Top" AllowAutomaticDeletes="True" AllowAutomaticInserts="True" AllowAutomaticUpdates="True" AutoGenerateColumns="False" 
                                            CellSpacing="-1" GridLines="None" OnItemInserted="RadGrid_Courses_ItemInserted" OnItemUpdated="RadGrid_Courses_ItemUpdated" OnItemDeleted="RadGrid_Courses_ItemDeleted">
                                            <MasterTableView DataSourceID="SqlDataSource_CourseTranslations" DataKeyNames="ID,ClientID,CourseID,PhotoLink" NoMasterRecordsText="No Translations for Course Found">
                                                <Columns>
                                                    <telerik:GridTemplateColumn DataField="Data" AllowFiltering="false" HeaderText="Course Image" UniqueName="Upload" ItemStyle-Width="10%" ItemStyle-HorizontalAlign="Center" HeaderStyle-Width="1%">
                                                        <ItemTemplate>
                                                            <telerik:RadBinaryImage runat="server" ID="RadBinaryImage1" ImageUrl='<%#Eval("PhotoLink") %>' AutoAdjustImageControlSize="false" Height="40px" Width="40px" />
                                                        </ItemTemplate>
                                                    </telerik:GridTemplateColumn>
                                                    <telerik:GridBoundColumn DataField="Title" HeaderText="Title" SortExpression="Title" UniqueName="Title" FilterControlAltText="Filter Title column"/>
                                                    <telerik:GridBoundColumn DataField="Subject" HeaderText="Subject" SortExpression="Subject" UniqueName="Subject" FilterControlAltText="Filter Subject column"/>
                                                    <%--<telerik:GridBoundColumn DataField="Overview" HeaderText="Overview" SortExpression="Overview" UniqueName="Overview" FilterControlAltText="Filter Overview column"/>--%>
                                                    <telerik:GridDropDownColumn DataField="LanguageID" HeaderText="Language" DataSourceID="SqlDataSource_Languages" ListTextField="Description" ListValueField="ID" UniqueName="LanguageID" FilterControlAltText="Filter LanguageID column"/>
                                                    <telerik:GridButtonColumn CommandName="Edit" Text="Edit" UniqueName="EditButton" ButtonType="PushButton" ButtonCssClass="btn btn-success btn-xs"  HeaderStyle-Width="1%" />
                                                    <telerik:GridButtonColumn CommandName="Delete" Text="Delete" UniqueName="DeleteButton" ButtonType="PushButton" ButtonCssClass="btn btn-danger btn-xs"  HeaderStyle-Width="1%" />                                                    
                                                </Columns>
                                                <NestedViewTemplate>
                                                    <div style="margin:3px">
                                                        <label>Overview:</label>
                                                        <br />
                                                        <asp:Label ID="Label1" runat="server" Text='<%# Bind( "Overview") %>'/> 
                                                        <br />                                                                                                 
                                                    </div>
                                                </NestedViewTemplate>
                                                <EditFormSettings EditFormType="Template">
                                                    <FormTemplate>
                                                        <table cellpadding="3px">
                                                             <tr>
                                                                <td><label>Upload New Image:</label><label style="font-size:smaller; color:grey;"> (Max File Size 1MB)</label> </td>
                                                                <td><telerik:RadAsyncUpload runat="server" ID="AsyncUpload" AllowedFileExtensions="jpg,jpeg,png,gif,bmp" MultipleFileSelection="Disabled" Skin="Windows7" MaxFileSize="1000000"/></td>
                                                            </tr>
                                                            <tr>
                                                                <td><label>Title: </label></td>
                                                                <td><telerik:RadTextBox ID="TextBox1" runat="server" Text='<%# Bind( "Title") %>' CssClass="form-control"/></td>
                                                            </tr> 
                                                            <tr>
                                                                <td><label>Subject: </label></td>
                                                                <td><telerik:RadTextBox ID="RadTextBox3" runat="server" Text='<%# Bind( "Subject") %>' CssClass="form-control"/></td>
                                                            </tr> 
                                                            <tr>
                                                                <td><label>Language:</label></td>
                                                                <td><telerik:RadComboBox ID="RadComboBox1" runat="server" DataSourceID="SqlDataSource_Languages" DataTextField="Description" DataValueField="ID" SelectedValue='<%# Bind("LanguageID") %>' Filter="Contains" DropDownAutoWidth="Enabled" /></td>
                                                            </tr>                                                
                                                         </table>
                                                        <div style="margin:3px">
                                                            <label>Message: </label><br />
                                                            <telerik:RadTextBox ID="RadTextBox1" runat="server" Text='<%# Bind( "Overview") %>' TextMode="MultiLine" Rows="8" Width="60%"/>
                                                        </div>
                                                        <hr />
                                                        <asp:Button ID="Button_Update" runat="server" style="margin:5px;" CssClass="btn btn-success btn-sm" Text='<%# (Container is GridEditFormInsertItem) ? "Insert" : "Update" %>' CommandName='<%# (Container is GridEditFormInsertItem) ? "PerformInsert" : "Update" %>' />
                                                        <asp:Button ID="Button_Cancel" runat="server" style="margin:5px;" CssClass="btn btn-danger btn-sm" Text="Cancel" CommandName="Cancel" CausesValidation="false" />
                                                    </FormTemplate>
                                                 </EditFormSettings>
                                            </MasterTableView>
                                        </telerik:RadGrid>
                                    </div>
                                 </div>

                            </div>
                            <div class="col-sm-6">     
                                <div class="row">
                                    <div class="col-sm-6">
                                        <h3>Video Playlist</h3>
                                    </div>
                                    <div class="col-sm-6">
                                        <br />
                                        <asp:Button ID="Button_AddCourseVideo" runat="server" Text="Add Video" style="float:right;" OnClick="Button_AddCourseVideo_Click" CssClass="btn btn-primary" />
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-sm-12">
                                        <telerik:RadGrid ID="RadGrid_CourseVideos" runat="server" DataSourceID="SqlDataSource_CourseVideos" GroupPanelPosition="Top" AllowAutomaticDeletes="True" AllowAutomaticInserts="True" AllowAutomaticUpdates="True" 
                                            AutoGenerateColumns="False" OnItemCommand="RadGrid_CourseVideos_ItemCommand">
                                            <MasterTableView DataSourceID="SqlDataSource_CourseVideos" DataKeyNames="ID,ClientID,CourseID,Position" NoMasterRecordsText="Playlist is Empty" EditMode="InPlace">
                                                <Columns>
                                                    <telerik:GridBoundColumn DataField="Position" HeaderText="#"  SortExpression="Position" UniqueName="Position" DataType="System.Int32" FilterControlAltText="Filter Position column" ReadOnly="true" HeaderStyle-Width="1%"/>
                                                    <telerik:GridTemplateColumn>
                                                        <ItemTemplate>
                                                            <asp:Label ID="Label2" runat="server" Text=<%#Eval("Title") %>></asp:Label>
                                                        </ItemTemplate>
                                                        <InsertItemTemplate>
                                                            <telerik:RadComboBox ID="RadC" runat="server" DataSourceID="SqlDataSource_ClientVideos" DataTextField="Title" DataValueField="ID" EmptyMessage="Select Video" SelectedValue='<%# Bind("VideoHeaderID") %>' Skin="Metro" Filter="Contains" Width="100%"></telerik:RadComboBox>
                                                        </InsertItemTemplate>
                                                    </telerik:GridTemplateColumn>                                                    
                                                    <telerik:GridButtonColumn CommandName="MoveUp" Text="▲ Move Up" ShowInEditForm="false" UniqueName="MoveUpButton" ButtonType="PushButton" ButtonCssClass="btn btn-primary btn-xs"  HeaderStyle-Width="1%" />
                                                    <telerik:GridButtonColumn CommandName="MoveDown" Text="▼ Move Down" ShowInEditForm="false" UniqueName="MoveDownButton" ButtonType="PushButton" ButtonCssClass="btn btn-primary btn-xs"  HeaderStyle-Width="1%" />
                                                    <telerik:GridTemplateColumn HeaderStyle-Width="1px" >
                                                        <ItemTemplate>
                                                            <asp:LinkButton runat="server" CommandName="Delete" CssClass="btn btn-xs btn-danger" ForeColor="White">Delete</asp:LinkButton>
                                                        </ItemTemplate>  
                                                        <InsertItemTemplate>
                                                            <asp:LinkButton runat="server" CommandName="PerformInsert" CssClass="btn btn-xs btn-info" ForeColor="White">Insert</asp:LinkButton>
                                                        </InsertItemTemplate>                          
                                                    </telerik:GridTemplateColumn>
                                                    <telerik:GridTemplateColumn HeaderStyle-Width="1px">                                                        
                                                        <InsertItemTemplate>
                                                            <asp:LinkButton runat="server" CommandName="Cancel" CssClass="btn btn-xs btn-warning" ForeColor="White">Cancel</asp:LinkButton>
                                                        </InsertItemTemplate>                          
                                                    </telerik:GridTemplateColumn>
                                                </Columns>
                                            </MasterTableView>
                                        </telerik:RadGrid>
                                    </div>
                                 </div>
                            </div>
                            
                        </telerik:RadPageView>

                        
                        <telerik:RadPageView ID="VideosPage" runat="server" Width="100%">
                            <div class="row">
                                <div class="col-sm-6">
                                    <h3>Video Headers</h3>
                                </div>
                                <div class="col-sm-6">
                                    <br />
                                    <asp:Button ID="ButtonNewVideo" runat="server" Text="Add New Video Header" style="float:right;" OnClick="ButtonNewVideo_Click"  CssClass="btn btn-primary"/>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-sm-12">
                                    <telerik:RadGrid ID="RadGrid_Videos" runat="server" DataSourceID="SqlDataSource_Videos" GroupPanelPosition="Top" AllowPaging="True" AllowAutomaticDeletes="True" AllowAutomaticInserts="True" AllowAutomaticUpdates="True" AutoGenerateColumns="False" CellSpacing="-1" GridLines="Both">
                                        <ClientSettings EnablePostBackOnRowClick="true">
                                            <Selecting AllowRowSelect="True"></Selecting>
                                        </ClientSettings>
                                        <MasterTableView DataSourceID="SqlDataSource_Videos" DataKeyNames="ID,ClientID" NoMasterRecordsText="No Video Headers Found">
                                            <Columns>                                               
                                                <telerik:GridBoundColumn DataField="Title" HeaderText="Title" SortExpression="Title" UniqueName="Title" FilterControlAltText="Filter Title column"/>                                                                                                    
                                                <telerik:GridButtonColumn CommandName="Edit" Text="Rename" UniqueName="EditButton" ButtonType="PushButton" ButtonCssClass="btn btn-primary btn-xs"  HeaderStyle-Width="1%" />
                                                <telerik:GridButtonColumn CommandName="Delete" Text="Delete" UniqueName="DeleteButton" ConfirmText="Are you sure you want to delete this video?" ConfirmDialogType="Classic" ButtonType="PushButton" ButtonCssClass="btn btn-danger btn-xs"  HeaderStyle-Width="1%" />                                                    
                                            </Columns>
                                            <EditFormSettings EditFormType="Template">
                                                    <FormTemplate>
                                                        <table cellpadding="3px">
                                                            <tr>
                                                                <td><label>Title: </label></td>
                                                                <td><telerik:RadTextBox ID="TextBox1" runat="server" Text='<%# Bind( "Title") %>' CssClass="form-control"/></td>
                                                            </tr>   
                                                        </table>                                                            
                                                        <asp:Button ID="Button_Update" runat="server" style="margin:5px;" CssClass="btn btn-success btn-sm" Text='<%# (Container is GridEditFormInsertItem) ? "Insert" : "Update" %>' CommandName='<%# (Container is GridEditFormInsertItem) ? "PerformInsert" : "Update" %>' />
                                                        <asp:Button ID="Button_Cancel" runat="server" style="margin:5px;" CssClass="btn btn-danger btn-sm" Text="Cancel" CommandName="Cancel" CausesValidation="false"  />
                                                    </FormTemplate>
                                                 </EditFormSettings>
                                        </MasterTableView>
                                    </telerik:RadGrid>
                                </div>
                            </div>
                            <hr />
                            <div class="row">
                                <div class="col-sm-6">                                    
                                    <h3>Video Links</h3>
                                </div>
                                <div class="col-sm-6">
                                    <br />
                                    <asp:Button ID="Button_NewVideoItem" runat="server" Text="Add New Video Item" style="float:right;" OnClick="Button_NewVideoItem_Click" CssClass="btn btn-primary" />
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-sm-12">
                                    <telerik:RadGrid ID="RadGrid_VideoItems" runat="server" DataSourceID="SqlDataSource_VideoItems" GroupPanelPosition="Top" AllowAutomaticDeletes="True" AllowAutomaticInserts="True" AllowAutomaticUpdates="True" AutoGenerateColumns="False" OnItemDataBound="RadGrid_VideoItems_ItemCreated">
                                        <MasterTableView DataSourceID="SqlDataSource_VideoItems" DataKeyNames="ID,ClientID,VideoHeaderID" NoMasterRecordsText="No links for Video Header Found">
                                            <Columns>
                                                <telerik:GridBoundColumn DataField="Title" HeaderText="Title" SortExpression="Title" UniqueName="Title" FilterControlAltText="Filter Title column"/>
                                                <telerik:GridDropDownColumn DataField="Language" HeaderText="Language" DataSourceID="SqlDataSource_Languages" ListTextField="Description" ListValueField="ID" UniqueName="Language" FilterControlAltText="Filter Language column"/>
                                                <%--<telerik:GridBoundColumn DataField="Script" HeaderText="Script" SortExpression="Script" UniqueName="Script" FilterControlAltText="Filter Script column"/>--%>
                                                <telerik:GridBoundColumn DataField="VideoLink" HeaderText="Video Link" SortExpression="VideoLink" UniqueName="VideoLink" FilterControlAltText="Filter VideoLink column"/>
                                                <telerik:GridDateTimeColumn DataField="LastUpdated" HeaderText="Last Updated" SortExpression="LastUpdated" UniqueName="LastUpdated" DataType="System.DateTime" FilterControlAltText="Filter LastUpdated column" DataFormatString="{0:dd/MM/yyyy}" DefaultInsertValue="01/01/1970"  />
                                                <telerik:GridButtonColumn CommandName="Edit" Text="Edit" UniqueName="EditButton" ButtonType="PushButton" ButtonCssClass="btn btn-primary btn-xs"  HeaderStyle-Width="1%" />
                                                <telerik:GridButtonColumn CommandName="Delete" Text="Delete" UniqueName="DeleteButton" ConfirmText="Are you sure you want to delete this video item?" ConfirmDialogType="Classic" ButtonType="PushButton" ButtonCssClass="btn btn-danger btn-xs"  HeaderStyle-Width="1%" />
                                            </Columns>
                                            <NestedViewTemplate>
                                                    <div style="margin:3px">
                                                        <label>Script:</label>
                                                        <br />
                                                        <asp:Label ID="Label1" runat="server" Text='<%# Bind( "Script") %>'/> 
                                                        <br />                                                                                                 
                                                    </div>
                                                </NestedViewTemplate>
                                                <EditFormSettings EditFormType="Template">
                                                    <FormTemplate>
                                                        <table cellpadding="3px">
                                                            <tr>
                                                                <td><label>Title: </label></td>
                                                                <td><telerik:RadTextBox ID="TextBox1" runat="server" Text='<%# Bind( "Title") %>' CssClass="form-control"/></td>
                                                            </tr>   
                                                            <tr>
                                                                <td><label>Language:</label></td>
                                                                <td><telerik:RadComboBox ID="RadComboBox1" runat="server" DataSourceID="SqlDataSource_Languages" DataTextField="Description" DataValueField="ID" SelectedValue='<%# Bind("Language") %>' Filter="Contains" DropDownAutoWidth="Enabled" /></td>
                                                            </tr>
                                                            <tr>
                                                                <td><label>Video Link: </label></td>
                                                                <td><telerik:RadTextBox ID="RadTextBox2" runat="server" Text='<%# Bind( "VideoLink") %>' CssClass="form-control"/></td>
                                                            </tr>
                                                            <tr>
                                                                <td><label>Last Updated: </label></td>
                                                                <td><telerik:RadDatePicker ID="RadDatePicker" runat="server" SelectedDate='<%# Bind("LastUpdated") %>'  MinDate="01/01/1970" /></td>
                                                            </tr>                                                
                                                         </table>
                                                        <div style="margin:3px">
                                                            <label>Script: </label><br />
                                                            <telerik:RadTextBox ID="RadTextBox1" runat="server" Text='<%# Bind( "Script") %>' TextMode="MultiLine" Rows="8" Width="100%" style="max-width:500px;"/>
                                                        </div>
                                                        <hr />
                                                        <asp:Button ID="Button_Update" runat="server" style="margin:5px;" CssClass="btn btn-success btn-sm" Text='<%# (Container is GridEditFormInsertItem) ? "Insert" : "Update" %>' CommandName='<%# (Container is GridEditFormInsertItem) ? "PerformInsert" : "Update" %>' />
                                                        <asp:Button ID="Button_Cancel" runat="server" style="margin:5px;" CssClass="btn btn-danger btn-sm" Text="Cancel" CommandName="Cancel" CausesValidation="false"  />
                                                    </FormTemplate>
                                                 </EditFormSettings>
                                        </MasterTableView>
                                    </telerik:RadGrid>
                                </div>
                            </div>

                        </telerik:RadPageView>

                        <telerik:RadPageView ID="LanguagesPage" runat="server" Width="100%">
                            <div class="row">
                                <div class="col-sm-6">                                    
                                    <h3>Languages</h3>
                                </div>
                                <div class="col-sm-6">
                                    <br />
                                    <asp:Button ID="Button_NewLanguage" runat="server" Text="Add New Language" style="float:right;" OnClick="Button_NewLanguage_Click" CssClass="btn btn-primary" />
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-sm-12">
                                    <telerik:RadGrid ID="RadGrid_Languages" runat="server" AllowAutomaticDeletes="True" AllowAutomaticInserts="True" AllowAutomaticUpdates="True" AutoGenerateColumns="False" DataSourceID="SqlDataSource_ClientLanguages" GroupPanelPosition="Top">
                                        <MasterTableView DataSourceID="SqlDataSource_ClientLanguages" DataKeyNames="ID,ClientID" NoMasterRecordsText="No languages assigned ">
                                            <Columns>
                                                <telerik:GridBoundColumn DataField="Code" HeaderText="Language Code" SortExpression="Code" UniqueName="Code" FilterControlAltText="Filter Code column" ReadOnly="true" HeaderStyle-Width="120px"/>
                                                <telerik:GridDropDownColumn DataField="LanguageID" HeaderText="Full Name" DataSourceID="SqlDataSource_AllLanguages" ListTextField="Description" ListValueField="ID" UniqueName="LanguageID" FilterControlAltText="Filter LanguageID column"/>
                                                <telerik:GridButtonColumn CommandName="Edit" Text="Edit" UniqueName="EditButton" ButtonType="PushButton" ButtonCssClass="btn btn-primary btn-xs"  HeaderStyle-Width="1%" />
                                                <telerik:GridButtonColumn CommandName="Delete" Text="Delete" UniqueName="DeleteButton" ConfirmText="Are you sure you want to delete this language?" ConfirmDialogType="Classic" ButtonType="PushButton" ButtonCssClass="btn btn-danger btn-xs"  HeaderStyle-Width="1%" />
                                            </Columns>
                                            <EditFormSettings EditFormType="Template">
                                                    <FormTemplate>
                                                        <table cellpadding="3px">                                                              
                                                            <tr>
                                                                <td><label>Language:</label></td>
                                                                <td><telerik:RadComboBox ID="RadComboBox1" runat="server" DataSourceID="SqlDataSource_AllLanguages" DataTextField="Description" DataValueField="ID" SelectedValue='<%# Bind("LanguageID") %>' Filter="Contains" DropDownAutoWidth="Enabled" /></td>
                                                            </tr>                                                                                                            
                                                         </table>                                                        
                                                        <hr />
                                                        <asp:Button ID="Button_Update" runat="server" style="margin:5px;" CssClass="btn btn-success btn-sm" Text='<%# (Container is GridEditFormInsertItem) ? "Insert" : "Update" %>' CommandName='<%# (Container is GridEditFormInsertItem) ? "PerformInsert" : "Update" %>' />
                                                        <asp:Button ID="Button_Cancel" runat="server" style="margin:5px;" CssClass="btn btn-danger btn-sm" Text="Cancel" CommandName="Cancel" CausesValidation="false"  />
                                                    </FormTemplate>
                                            </EditFormSettings>
                                        </MasterTableView>
                                    </telerik:RadGrid>
                                </div>
                            </div>
                        </telerik:RadPageView>


                        <telerik:RadPageView ID="AccountsPage" runat="server" Width="100%">
                            <div class="row">
                                <div class="col-sm-6">                                    
                                    <h3>Users Logins</h3>
                                </div>
                                <div class="col-sm-6">
                                    <br />
                                    <a class="btn btn-info"  href="#" style="float:right; margin-left:5px;" onclick="RefreshGrid()"> Refresh </a>
                                    <asp:Button ID="Button_NewUser" runat="server" Text="Add New User" style="float:right;" OnClientClick="OpenNewUserWindow()" CssClass="btn btn-primary" />
                                    
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-sm-12">
                                    <telerik:RadGrid ID="RadGrid_Users" runat="server" DataSourceID="SqlDataSource_Users" AllowPaging="True" AutoGenerateColumns="False" CellSpacing="-1"
                                        PageSize="30" GroupPanelPosition="Top" AllowAutomaticDeletes="True" AllowAutomaticUpdates="True">
                                        <MasterTableView DataKeyNames="Id,UserName" DataSourceID="SqlDataSource_Users" NoMasterRecordsText="No client users found">
                                            <Columns>                                                
                                                <telerik:GridBoundColumn DataField="UserName" HeaderText="Email Address" SortExpression="UserName" UniqueName="UserName" FilterControlAltText="Filter UserName column" />
                                                <telerik:GridButtonColumn CommandName="Edit" Text="Change Email" UniqueName="EditButton" ButtonType="PushButton" ButtonCssClass="btn btn-primary btn-xs"  HeaderStyle-Width="1%" />
                                                <telerik:GridTemplateColumn HeaderStyle-Width="1px">
                                                    <ItemTemplate>
                                                        <asp:LinkButton runat="server" ForeColor="White" OnClientClick='<%# string.Format("javascript:OpenRestWindow(\"{0}\");", Eval("UserName").ToString()) %>'  CssClass="btn btn-primary btn-xs">
                                                            Reset Password
                                                        </asp:LinkButton>
                                                    </ItemTemplate>
                                                </telerik:GridTemplateColumn>
                                                
                                                <telerik:GridButtonColumn CommandName="Delete" ButtonType="PushButton" Text="Delete User" UniqueName="DeleteButton" ButtonCssClass="btn btn-danger btn-xs" ConfirmText="Are you sure you want to delete this user?" ConfirmDialogType="RadWindow" Reorderable="False" HeaderStyle-Width="1%" />
                                            </Columns>
                                            <EditFormSettings EditFormType="Template">
                                                    <FormTemplate>
                                                        <table cellpadding="3px">                                                              
                                                            <tr>
                                                                <td><label>Email Address:</label></td>
                                                                <td><telerik:RadTextBox ID="TextBox1" runat="server" Text='<%# Bind( "UserName") %>' CssClass="form-control"/></td>
                                                            </tr>                                                                                                            
                                                         </table>                                                        
                                                        <hr />
                                                        <asp:Button ID="Button_Update" runat="server" style="margin:5px;" CssClass="btn btn-success btn-sm" Text='<%# (Container is GridEditFormInsertItem) ? "Insert" : "Update" %>' CommandName='<%# (Container is GridEditFormInsertItem) ? "PerformInsert" : "Update" %>' />
                                                        <asp:Button ID="Button_Cancel" runat="server" style="margin:5px;" CssClass="btn btn-danger btn-sm" Text="Cancel" CommandName="Cancel" CausesValidation="false"  />
                                                    </FormTemplate>
                                            </EditFormSettings>
                                        </MasterTableView>
                                        <%--<ClientSettings>
                                            <ClientEvents OnCommand="RaiseCommand" />
                                        </ClientSettings>--%>
                                    </telerik:RadGrid>
                                </div>
                            </div>
                        </telerik:RadPageView>
                    </telerik:RadMultiPage>
                </div>
            </div>
        </div>
    </div>

    <%--Error Telerik Rad Window Manager. Used to display Popup Dialogs and the Reset Password Window--%>
    <telerik:RadWindowManager ID="RadWindowManager1" Skin="Simple" runat="server">
        <Windows>
            <%--Here is the reset password window--%>
            <telerik:RadWindow ID="RadWindow1" runat="server" ShowContentDuringLoad="false" Width="400px"
                Height="400px" Title="" Behaviors="Move,Close" OnClientClose="RefreshGrid">
            </telerik:RadWindow>

            <telerik:RadWindow ID="RadWindow_Videos" runat="server" Style="display: none;" MinWidth="400" MinHeight="400" Modal="true" Width="400" Height="600"
                                Left="0" Top="0" EnableShadow="true" DestroyOnClose="true" ReloadOnShow="true" Visible="true" Behaviors="Move,Close"
                                VisibleOnPageLoad="false" Title="Video Headers" Overlay="True" KeepInScreenBounds="True">
                <ContentTemplate>
                    <div class="container">
                        <div class="row" style="margin:10px;">
                            <div class="col-sm-12">
                                <h3>Video Headers</h3>
                                <hr />
                            </div>
                            <div class="col=sm-12">
                                <telerik:RadGrid ID="RadGrid_VideoList" runat="server" AllowAutomaticDeletes="True" AutoGenerateColumns="False" DataSourceID="SqlDataSource_ClientVideos" GroupPanelPosition="Top" CellSpacing="-1" OnDeleteCommand="RadGrid_VideoList_Add" AllowFilteringByColumn="True" AllowPaging="True" AllowSorting="True" PageSize="30">
                                    <MasterTableView DataSourceID="SqlDataSource_ClientVideos" DataKeyNames="ID" NoMasterRecordsText="No videos found">
                                        <Columns>
                                            <telerik:GridBoundColumn DataField="Title" HeaderText="Video Header Title" SortExpression="Title" UniqueName="Title" FilterControlAltText="Filter Title column"/>                                                                                                    
                                            <telerik:GridButtonColumn CommandName="Delete" Text="Add Video" UniqueName="DeleteButton" ButtonType="PushButton" ButtonCssClass="btn btn-primary btn-xs"  HeaderStyle-Width="1%" />
                                        </Columns>
                                    </MasterTableView>                                
                                </telerik:RadGrid>
                            </div>
                        </div>
                    </div>
                </ContentTemplate>
            </telerik:RadWindow>

        </Windows>
    </telerik:RadWindowManager>

    <telerik:RadCodeBlock ID="RadCodeBlock1" runat="server">
        <script type="text/javascript">
            //This Javasript function opens the Reset Password Page in a Popout window.
            //The Username is passed as a request parameter.
            function OpenRestWindow(UserName) {
                
                var url = "/UserPages/ResetPasswordForm?User=" + UserName;
                radopen(url, "RadWindow1", 400, 550, 100, 100);
            }

            function OpenNewUserWindow(ClientID) {
                var url = "/UserPages/NewUserForm?ClientID=" + getURLParameter("ID");
                radopen(url, "RadWindow1", 400, 650, 100, 100);
            }

            function RefreshGrid() {

                setTimeout(function () {
                    var masterTable = $find("<%=RadGrid_Users.ClientID%>").get_masterTableView();
                    masterTable.rebind();
                }, 500);
                
            }

            function OpenVideosWindow() {
                $find("<%=RadWindow_Videos.ClientID %>").show();
            }

           
            function OnAddedVideo(sender, eventArgs) {
                var masterTable = $find("<%=RadGrid_Users.ClientID%>").get_masterTableView();
                masterTable.rebind();
            }

            

            function getURLParameter(name) {
                return decodeURIComponent((new RegExp('[?|&]' + name + '=' + '([^&;]+?)(&|#|;|$)').exec(location.search)||[,""])[1].replace(/\+/g, '%20'))||null
            }

         </script>
    </telerik:RadCodeBlock>

    <asp:SqlDataSource runat="server" ID="SqlDataSource_ClientInfo" ConnectionString='<%$ ConnectionStrings:DefaultConnection %>' SelectCommand="SELECT * FROM [Clients] WHERE ([ID] = @ID)">
        <SelectParameters>
            <asp:QueryStringParameter QueryStringField="ID" Name="ID" Type="Int32"></asp:QueryStringParameter>
        </SelectParameters>
    </asp:SqlDataSource>

    <asp:SqlDataSource runat="server" ID="SqlDataSource_Courses" ConnectionString='<%$ ConnectionStrings:DefaultConnection %>' 
        SelectCommand="SELECT * FROM [CourseHeaders] WHERE ([ClientID] = @ClientID)" 
        DeleteCommand="DELETE FROM CourseHeaders WHERE (ID = @ID) AND (@ClientID = @ClientID)" 
        InsertCommand="INSERT INTO CourseHeaders(ClientID, Title) VALUES (@ClientID, @Title);" 
        UpdateCommand="UPDATE CourseHeaders SET Title = @Title WHERE (ID = @ID) AND (ClientID = @ClientID)">
        <DeleteParameters>
            <asp:Parameter Name="ID"></asp:Parameter>
            <asp:QueryStringParameter QueryStringField="ID" Name="ClientID" Type="Int32"></asp:QueryStringParameter>
        </DeleteParameters>
        <InsertParameters>
            <asp:QueryStringParameter QueryStringField="ID" Name="ClientID" Type="Int32"></asp:QueryStringParameter>
            <asp:Parameter Name="Title"></asp:Parameter>
            <%--<asp:Parameter Name="NewID" Type="Int32"></asp:Parameter>--%>
        </InsertParameters>
        <SelectParameters>
            <asp:QueryStringParameter QueryStringField="ID" Name="ClientID" Type="Int32"></asp:QueryStringParameter>
        </SelectParameters>
        <UpdateParameters>
            <asp:Parameter Name="Title"></asp:Parameter>
            <asp:Parameter Name="ID"></asp:Parameter>
            <asp:QueryStringParameter QueryStringField="ID" Name="ClientID" Type="Int32"></asp:QueryStringParameter>
        </UpdateParameters>
    </asp:SqlDataSource>

    <asp:SqlDataSource ID="SqlDataSource_CourseTranslations" runat="server" ConnectionString='<%$ ConnectionStrings:DefaultConnection %>' 
        SelectCommand="SELECT * FROM [CourseTranslations] WHERE (([ClientID] = @ClientID) AND ([CourseID] = @CourseID))" 
        DeleteCommand="DELETE FROM CourseTranslations WHERE (ID = @ID) AND (ClientID = @ClientID)" 
        InsertCommand="INSERT INTO CourseTranslations(ClientID, CourseID, Title, Subject , Overview, LanguageID) VALUES (@ClientID, @CourseID, @Title, @Subject, @Overview, @LanguageID)" 
        UpdateCommand="UPDATE CourseTranslations SET Title = @Title, Subject = @Subject, Overview = @Overview, LanguageID = @LanguageID WHERE (ID = @ID)">
        <DeleteParameters>
            <asp:Parameter Name="ID"></asp:Parameter>
            <asp:ControlParameter ControlID="RadGrid_Courses" Name="CourseID" Type="Int32" PropertyName="SelectedValues['ID']"></asp:ControlParameter>
        </DeleteParameters>
        <InsertParameters>
            <asp:QueryStringParameter QueryStringField="ID" Name="ClientID" Type="Int32"></asp:QueryStringParameter>
            <asp:ControlParameter ControlID="RadGrid_Courses" Name="CourseID" Type="Int32" PropertyName="SelectedValues['ID']"></asp:ControlParameter>
            <asp:Parameter Name="Title"></asp:Parameter>
            <asp:Parameter Name="Subject"></asp:Parameter>
            <asp:Parameter Name="LanguageID"></asp:Parameter>
        </InsertParameters>
        <SelectParameters>
            <asp:QueryStringParameter QueryStringField="ID" Name="ClientID" Type="Int32"></asp:QueryStringParameter>
            <asp:ControlParameter ControlID="RadGrid_Courses" Name="CourseID" Type="Int32" PropertyName="SelectedValues['ID']" DefaultValue="-1"></asp:ControlParameter>
        </SelectParameters>
        <UpdateParameters>
            <asp:Parameter Name="Title"></asp:Parameter>
            <asp:Parameter Name="Subject"></asp:Parameter>
            <asp:Parameter Name="LanguageID"></asp:Parameter>
            <asp:Parameter Name="ID"></asp:Parameter>
        </UpdateParameters>
    </asp:SqlDataSource>

    <asp:SqlDataSource ID="SqlDataSource_CourseVideos" runat="server" ConnectionString='<%$ ConnectionStrings:DefaultConnection %>'
        SelectCommand="SELECT CourseItems.*, Title FROM CourseItems LEFT JOIN VideoHeaders ON CourseItems.VideoHeaderID = VideoHeaders.ID WHERE ((CourseItems.ClientID = @ClientID) AND ([CourseID] = @CourseID)) ORDER BY Position"
        DeleteCommand="DeleteCourseVideo" DeleteCommandType="StoredProcedure"
        InsertCommand="InsertCourseVideo" InsertCommandType="StoredProcedure" >
        <DeleteParameters>
            <asp:Parameter Name="ID"></asp:Parameter>
            <asp:QueryStringParameter QueryStringField="ID" Name="ClientID" Type="Int32"></asp:QueryStringParameter>
            <asp:Parameter Name="CourseID" Type="Int32"></asp:Parameter>
            <asp:Parameter Name="Position" Type="Int32"></asp:Parameter>
        </DeleteParameters>
        <InsertParameters>
            <asp:Parameter Name="ID" Type="Int32"></asp:Parameter>
            <asp:Parameter Name="VideoHeaderID"></asp:Parameter>
            <asp:QueryStringParameter QueryStringField="ID" Name="ClientID" Type="Int32"></asp:QueryStringParameter>
            <asp:ControlParameter ControlID="RadGrid_Courses" Name="CourseID" Type="Int32" PropertyName="SelectedValues['ID']"></asp:ControlParameter>
            <asp:Parameter Name="Position" DefaultValue="0" Type="Int32"></asp:Parameter>
            <asp:Parameter Name="Title" Type="String"></asp:Parameter>
        </InsertParameters>
        <SelectParameters>
            <asp:QueryStringParameter QueryStringField="ID" Name="ClientID" Type="Int32"></asp:QueryStringParameter>
            <asp:ControlParameter ControlID="RadGrid_Courses" Name="CourseID" Type="Int32" PropertyName="SelectedValues['ID']" DefaultValue="-1"></asp:ControlParameter>
        </SelectParameters>
    </asp:SqlDataSource>

    <asp:SqlDataSource ID="SqlDataSource_CourseVideosMove" runat="server" ConnectionString='<%$ ConnectionStrings:DefaultConnection %>' 
        SelectCommand="MoveCourseVideo" SelectCommandType="StoredProcedure">
        <SelectParameters>
            <asp:Parameter Name="ID1" Type="Int32"></asp:Parameter>
            <asp:Parameter Name="CourseID" Type="Int32"></asp:Parameter>
            <asp:Parameter Name="ClientID" Type="Int32"></asp:Parameter>
            <asp:Parameter Name="Pos1" Type="Int32"></asp:Parameter>
            <asp:Parameter Name="MoveBy" Type="Int32"></asp:Parameter>
        </SelectParameters>
    </asp:SqlDataSource>

    <asp:SqlDataSource ID="SqlDataSource_ClientVideos" runat="server" ConnectionString='<%$ ConnectionStrings:DefaultConnection %>' 
        SelectCommand="SELECT * FROM [VideoHeaders] WHERE ([ClientID] = @ClientID) ORDER BY Title" 
        DeleteCommand="InsertCourseVideo" DeleteCommandType="StoredProcedure">
        <DeleteParameters>
            <asp:Parameter Name="ID" Type="Int32"></asp:Parameter>
            <asp:QueryStringParameter QueryStringField="ID" Name="ClientID" Type="Int32"></asp:QueryStringParameter>
            <asp:ControlParameter ControlID="RadGrid_Courses" Name="CourseID" Type="Int32" PropertyName="SelectedValues['ID']" ></asp:ControlParameter>
        </DeleteParameters>
        <SelectParameters>
            <asp:QueryStringParameter QueryStringField="ID" Name="ClientID" Type="Int32" ></asp:QueryStringParameter>
        </SelectParameters>       
    </asp:SqlDataSource>

   <%-- <asp:SqlDataSource ID="SqlDataSource_ClientVideos" runat="server" ConnectionString='<%$ ConnectionStrings:DefaultConnection %>' 
        SelectCommand="SELECT [ID], [Title] FROM [VideoHeaders] WHERE ([ClientID] = @ClientID) ORDER BY [Title]">
        <SelectParameters>
            <asp:QueryStringParameter QueryStringField="ID" Name="ClientID" Type="Int32"></asp:QueryStringParameter>
        </SelectParameters>
    </asp:SqlDataSource>--%>


    <asp:SqlDataSource ID="SqlDataSource_Videos" runat="server" ConnectionString='<%$ ConnectionStrings:DefaultConnection %>'
        SelectCommand="SELECT * FROM [VideoHeaders] WHERE ([ClientID] = @ClientID) ORDER BY Title"
        DeleteCommand="DELETE FROM VideoHeaders WHERE (ID = @ID) AND (ClientID = @ClientID)" InsertCommand="INSERT INTO VideoHeaders(ClientID, Title) VALUES (@ClientID, @Title)" UpdateCommand="UPDATE VideoHeaders SET Title = @Title WHERE (ID = @ID)">
        <DeleteParameters>
            <asp:Parameter Name="ID"></asp:Parameter>
            <asp:QueryStringParameter QueryStringField="ID" Name="ClientID" Type="Int32"></asp:QueryStringParameter>
        </DeleteParameters>
        <InsertParameters>
            <asp:QueryStringParameter QueryStringField="ID" Name="ClientID" Type="Int32"></asp:QueryStringParameter>
            <asp:Parameter Name="Title"></asp:Parameter>
        </InsertParameters>
        <SelectParameters>
            <asp:QueryStringParameter QueryStringField="ID" Name="ClientID" Type="Int32"></asp:QueryStringParameter>
        </SelectParameters>
        <UpdateParameters>
            <asp:Parameter Name="Title"></asp:Parameter>
            <asp:Parameter Name="ID"></asp:Parameter>
        </UpdateParameters>
    </asp:SqlDataSource>

    <asp:SqlDataSource ID="SqlDataSource_VideoItems" runat="server" ConnectionString='<%$ ConnectionStrings:DefaultConnection %>' 
        SelectCommand="SELECT * FROM [VideoItems] WHERE (([ClientID] = @ClientID) AND ([VideoHeaderID] = @VideoHeaderID)) ORDER BY Language" 
        DeleteCommand="DELETE FROM VideoItems WHERE (ID = @ID) AND (@ClientID = @ClientID)" 
        InsertCommand="INSERT INTO VideoItems(Language, Title, Script, VideoLink, VideoHeaderID, ClientID, LastUpdated) VALUES (@Language, @Title, @Script, @VideoLink, @VideoHeaderID, @ClientID, @LastUpdated)" 
        UpdateCommand="UPDATE VideoItems SET Language = @Language, Title = @Title, Script = @Script, VideoLink = @VideoLink, LastUpdated = @LastUpdated  WHERE (ID = @ID) AND (ClientID = @ClientID)">
        <DeleteParameters>
            <asp:Parameter Name="ID"></asp:Parameter>
            <asp:QueryStringParameter QueryStringField="ID" Name="ClientID" Type="Int32"></asp:QueryStringParameter>
        </DeleteParameters>
        <InsertParameters>
            <asp:Parameter Name="LastUpdated"></asp:Parameter>
            <asp:Parameter Name="Language"></asp:Parameter>
            <asp:Parameter Name="Title"></asp:Parameter>
            <asp:Parameter Name="Script"></asp:Parameter>
            <asp:Parameter Name="VideoLink"></asp:Parameter>
            <asp:ControlParameter ControlID="RadGrid_Videos" Name="VideoHeaderID" Type="Int32" PropertyName="SelectedValues['ID']"></asp:ControlParameter>
            <asp:QueryStringParameter QueryStringField="ID" Name="ClientID" Type="Int32"></asp:QueryStringParameter>
        </InsertParameters>
        <SelectParameters>
            <asp:QueryStringParameter QueryStringField="ID" Name="ClientID" Type="Int32"></asp:QueryStringParameter>
            <asp:ControlParameter ControlID="RadGrid_Videos" Name="VideoHeaderID" Type="Int32" PropertyName="SelectedValues['ID']" DefaultValue="-1"></asp:ControlParameter>
        </SelectParameters>
        <UpdateParameters>
            <asp:Parameter Name="LastUpdated"></asp:Parameter>
            <asp:Parameter Name="Language"></asp:Parameter>
            <asp:Parameter Name="Title"></asp:Parameter>
            <asp:Parameter Name="Script"></asp:Parameter>
            <asp:Parameter Name="VideoLink"></asp:Parameter>
            <asp:Parameter Name="ID"></asp:Parameter>
            <asp:QueryStringParameter QueryStringField="ID" Name="ClientID" Type="Int32"></asp:QueryStringParameter>
        </UpdateParameters>
    </asp:SqlDataSource>


    <asp:SqlDataSource ID="SqlDataSource_Languages" runat="server" ConnectionString='<%$ ConnectionStrings:DefaultConnection %>' 
        SelectCommand="SELECT Languages.Description, Languages.ID, Languages.Code FROM ClientLanguages LEFT OUTER JOIN Languages ON ClientLanguages.LanguageID = Languages.ID WHERE (ClientLanguages.ClientID = @ClientID)">
        <SelectParameters>
            <asp:QueryStringParameter QueryStringField="ID" Name="ClientID"></asp:QueryStringParameter>
        </SelectParameters>
    </asp:SqlDataSource>

    <asp:SqlDataSource ID="SqlDataSource_AllLanguages" runat="server" ConnectionString='<%$ ConnectionStrings:DefaultConnection %>' SelectCommand="SELECT ID, CONCAT(Code, ' - ', Description) AS Description FROM [Languages]"></asp:SqlDataSource>

    <asp:SqlDataSource ID="SqlDataSource_ClientLanguages" runat="server" ConnectionString='<%$ ConnectionStrings:DefaultConnection %>' 
        SelectCommand="SELECT ClientLanguages.*, Languages.Code FROM ClientLanguages LEFT JOIN Languages ON ClientLanguages.LanguageID = Languages.ID WHERE (ClientID = @ClientID)" 
        DeleteCommand="DELETE FROM ClientLanguages WHERE (ID = @ID) AND (ClientID = @ClientID)" 
        InsertCommand="INSERT INTO ClientLanguages(ClientID, LanguageID) VALUES (@ClientID, @LanguageID)" 
        UpdateCommand="UPDATE ClientLanguages SET LanguageID = @LanguageID WHERE (ID = @ID) AND (ClientID = @ClientID)">
        <DeleteParameters>
            <asp:Parameter Name="ID"></asp:Parameter>
            <asp:QueryStringParameter QueryStringField="ID" Name="ClientID"></asp:QueryStringParameter>
        </DeleteParameters>
        <InsertParameters>
            <asp:QueryStringParameter QueryStringField="ID" Name="ClientID"></asp:QueryStringParameter>
            <asp:Parameter Name="LanguageID"></asp:Parameter>
        </InsertParameters>
        <SelectParameters>
            <asp:QueryStringParameter QueryStringField="ID" Name="ClientID" Type="Int32"></asp:QueryStringParameter>
        </SelectParameters>
        <UpdateParameters>
            <asp:Parameter Name="LanguageID"></asp:Parameter>
            <asp:Parameter Name="ID"></asp:Parameter>
            <asp:QueryStringParameter QueryStringField="ID" Name="ClientID"></asp:QueryStringParameter>
        </UpdateParameters>
    </asp:SqlDataSource>

    <asp:SqlDataSource ID="SqlDataSource_Users" runat="server" ConnectionString='<%$ ConnectionStrings:DefaultConnection %>' 
        SelectCommand="SELECT AspNetUsers .* FROM AspNetUserClients LEFT JOIN AspNetUsers On AspNetUserClients.UserID = AspNetUsers.Id WHERE ClientID = @ClientID"
        DeleteCommand="DELETE FROM AspNetUsers WHERE (Id = @Id)" 
        UpdateCommand="UPDATE AspNetUsers SET Email = @UserName, UserName = @UserName WHERE (Id = @Id)">

        <DeleteParameters>
            <asp:Parameter Name="Id"></asp:Parameter>
        </DeleteParameters>
        <SelectParameters>
            <asp:QueryStringParameter QueryStringField="ID" Name="ClientID"></asp:QueryStringParameter>
        </SelectParameters>
        <UpdateParameters>
            <asp:Parameter Name="UserName"></asp:Parameter>
            <asp:Parameter Name="Id"></asp:Parameter>
        </UpdateParameters>
    </asp:SqlDataSource>



    <%-- AJAX Managers and Loading Panel --%>
    <telerik:RadAjaxLoadingPanel ID="RadAjaxLoadingPanel1"  runat="server" />
    
   <telerik:RadAjaxManager ID="RadAjaxManager1" DefaultLoadingPanelID="RadAjaxLoadingPanel1" runat="server">
        <AjaxSettings>
            <telerik:AjaxSetting AjaxControlID="RadGrid_Courses"><UpdatedControls><telerik:AjaxUpdatedControl ControlID="RadGrid_Courses" LoadingPanelID="RadAjaxLoadingPanel1" /></UpdatedControls></telerik:AjaxSetting>
            <telerik:AjaxSetting AjaxControlID="RadGrid_Courses"><UpdatedControls><telerik:AjaxUpdatedControl ControlID="RadGrid_CourseTitles" LoadingPanelID="RadAjaxLoadingPanel1" /></UpdatedControls></telerik:AjaxSetting>
            <telerik:AjaxSetting AjaxControlID="RadGrid_Courses"><UpdatedControls><telerik:AjaxUpdatedControl ControlID="RadGrid_CourseVideos" LoadingPanelID="RadAjaxLoadingPanel1" /></UpdatedControls></telerik:AjaxSetting>
            <telerik:AjaxSetting AjaxControlID="Button_NewCourse"><UpdatedControls><telerik:AjaxUpdatedControl ControlID="RadGrid_Courses" LoadingPanelID="RadAjaxLoadingPanel1" /></UpdatedControls></telerik:AjaxSetting>
            <telerik:AjaxSetting AjaxControlID="RadGrid_CourseTitles"><UpdatedControls><telerik:AjaxUpdatedControl ControlID="RadGrid_CourseTitles" LoadingPanelID="RadAjaxLoadingPanel1" /></UpdatedControls></telerik:AjaxSetting>
            <telerik:AjaxSetting AjaxControlID="Button_NewCourseTitle"><UpdatedControls><telerik:AjaxUpdatedControl ControlID="RadGrid_CourseTitles" LoadingPanelID="RadAjaxLoadingPanel1" /></UpdatedControls></telerik:AjaxSetting>
            <telerik:AjaxSetting AjaxControlID="RadGrid_CourseVideos"><UpdatedControls><telerik:AjaxUpdatedControl ControlID="RadGrid_CourseVideos" LoadingPanelID="RadAjaxLoadingPanel1" /></UpdatedControls></telerik:AjaxSetting>
            <telerik:AjaxSetting AjaxControlID="Button_AddCourseVideo"><UpdatedControls><telerik:AjaxUpdatedControl ControlID="RadGrid_CourseVideos" LoadingPanelID="RadAjaxLoadingPanel1" /></UpdatedControls></telerik:AjaxSetting>
            <telerik:AjaxSetting AjaxControlID="RadGrid_VideoList"><UpdatedControls><telerik:AjaxUpdatedControl ControlID="RadGrid_CourseVideos" LoadingPanelID="RadAjaxLoadingPanel1" /></UpdatedControls></telerik:AjaxSetting>
            <telerik:AjaxSetting AjaxControlID="RadGrid_VideoList"><UpdatedControls><telerik:AjaxUpdatedControl ControlID="RadGrid_VideoList" LoadingPanelID="RadAjaxLoadingPanel1" /></UpdatedControls></telerik:AjaxSetting>
            <telerik:AjaxSetting AjaxControlID="RadGrid_Videos"><UpdatedControls><telerik:AjaxUpdatedControl ControlID="RadGrid_Videos" LoadingPanelID="RadAjaxLoadingPanel1" /></UpdatedControls></telerik:AjaxSetting>
            <telerik:AjaxSetting AjaxControlID="RadGrid_Videos"><UpdatedControls><telerik:AjaxUpdatedControl ControlID="RadGrid_VideoItems" LoadingPanelID="RadAjaxLoadingPanel1" /></UpdatedControls></telerik:AjaxSetting>
            <telerik:AjaxSetting AjaxControlID="ButtonNewVideo"><UpdatedControls><telerik:AjaxUpdatedControl ControlID="RadGrid_Videos" LoadingPanelID="RadAjaxLoadingPanel1" /></UpdatedControls></telerik:AjaxSetting>
            <telerik:AjaxSetting AjaxControlID="RadGrid_VideoItems"><UpdatedControls><telerik:AjaxUpdatedControl ControlID="RadGrid_VideoItems" LoadingPanelID="RadAjaxLoadingPanel1" /></UpdatedControls></telerik:AjaxSetting>
            <telerik:AjaxSetting AjaxControlID="Button_NewVideoItem"><UpdatedControls><telerik:AjaxUpdatedControl ControlID="RadGrid_VideoItems" LoadingPanelID="RadAjaxLoadingPanel1" /></UpdatedControls></telerik:AjaxSetting>
            <telerik:AjaxSetting AjaxControlID="RadGrid_Languages"><UpdatedControls><telerik:AjaxUpdatedControl ControlID="RadGrid_Languages" LoadingPanelID="RadAjaxLoadingPanel1" /></UpdatedControls></telerik:AjaxSetting>
            <telerik:AjaxSetting AjaxControlID="Button_NewLanguage"><UpdatedControls><telerik:AjaxUpdatedControl ControlID="RadGrid_Languages" LoadingPanelID="RadAjaxLoadingPanel1" /></UpdatedControls></telerik:AjaxSetting>
            <telerik:AjaxSetting AjaxControlID="RadGrid_Users"><UpdatedControls><telerik:AjaxUpdatedControl ControlID="RadGrid_Users" LoadingPanelID="RadAjaxLoadingPanel1" /></UpdatedControls></telerik:AjaxSetting>
            <telerik:AjaxSetting AjaxControlID="Button_NewUser"><UpdatedControls><telerik:AjaxUpdatedControl ControlID="RadGrid_Users" LoadingPanelID="RadAjaxLoadingPanel1" /></UpdatedControls></telerik:AjaxSetting>
        </AjaxSettings>
    </telerik:RadAjaxManager>


</asp:Content>
