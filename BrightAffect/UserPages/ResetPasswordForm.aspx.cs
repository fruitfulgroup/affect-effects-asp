﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using Owin;

namespace BrightAffect.Account
{
    public partial class ResetPasswordForm : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            success_div.Visible = false;
            alert_div.Visible = false;


            try { TextBox_Username.Text = Request.QueryString["User"]; }
            catch { }
        }

        protected void ResetPassword_Click(object sender, EventArgs e)
        {
            if (IsValid)
            {
                try
                {
                    success_div.Visible = false;
                    alert_div.Visible = false;

                    //Try Get User account
                    var manager = Context.GetOwinContext().GetUserManager<ApplicationUserManager>();
                    var user = manager.FindByName(TextBox_Username.Text);
                    
                    if (user == null)
                    {
                        alert_div.Visible = true;
                        alert_message.Text = "User does not exist!";
                        return;
                    }

                    string code = manager.GeneratePasswordResetToken(user.Id);                    
                    IdentityResult result = manager.ResetPassword(user.Id, code, TextBox_Password.Text);
                    if (result.Succeeded)
                    {
                        success_div.Visible = true;
                    }
                    else
                    {
                        alert_div.Visible = true;
                        alert_message.Text = result.Errors.FirstOrDefault();
                    }
                }
                catch (Exception ex)
                {
                    alert_div.Visible = true;
                    alert_message.Text = "Unknown Error: " + ex.ToString();
                }
            }
        }

    }
}