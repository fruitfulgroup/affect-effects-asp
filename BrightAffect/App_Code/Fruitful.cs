﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Net.Mail;
using System.Net;
using Telerik.Web.UI;


public static class Fruitful
{
    public static string Get_Checked_Values_As_CSV(RadComboBox rBox)
    {
        string csv = string.Join(",", rBox.CheckedItems.Select(i => i.Value));
        return csv;
    }

    public static string GetCompany_ID(string UserID)
    {
        SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString);
        SqlCommand cmd = new SqlCommand("SELECT Company_ID from [AspNetUsers] where Id = @UserID", conn);
        cmd.Parameters.Add(new SqlParameter("@UserID", UserID));
        conn.Open();

        int id = Convert.ToInt32(cmd.ExecuteScalar());

        conn.Close();

        return Convert.ToString(id);
    }

    public static string getUserID(string EmailID)
    {
        SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString);
        SqlCommand cmd = new SqlCommand("SELECT Id from [AspNetUsers] where Email = @EmailID", conn);
        cmd.Parameters.Add(new SqlParameter("@EmailID", EmailID));
        conn.Open();

        string id = Convert.ToString(cmd.ExecuteScalar());

        conn.Close();

        return id;
    }

    public static void SendEmail(string email, string subject, string message)
    {
        try
        {

            MailMessage mailMessage = new MailMessage();
            mailMessage.To.Add(email);
            mailMessage.From = new MailAddress("noreply@snap-quotes.com");
            mailMessage.Subject = subject;
            mailMessage.Body = message;
            mailMessage.CC.Remove(new MailAddress("noreply@fruitfulgroup.com"));

            mailMessage.Bcc.Remove(new MailAddress("noreply@fruitfulgroup.com"));


            SmtpClient smtpClient = new SmtpClient("");
            NetworkCredential nc = new NetworkCredential("noreply@fruitfulgroup.com", "sqls9478h");
            smtpClient = new SmtpClient("smtp.gmail.com", 587);
            smtpClient.EnableSsl = true;

            smtpClient.Credentials = nc;
            smtpClient.Send(mailMessage);
        }
        catch
        {

        }

    }


    public static void Format_Aggregate_Headers(object sender, PivotGridCellDataBoundEventArgs e)
    {
        // Rename aggregates after the captions
        if (e.Cell is PivotGridHeaderCell && e.Cell.Field != null && e.Cell.Field.FieldType == "PivotGridAggregateField")
        {
            e.Cell.Text = e.Cell.Field.Caption;
        }
      
    }


    public static void Format_CurrencyDataCell(PivotGridCellDataBoundEventArgs e, string Currency)
    {
        if (e.Cell is PivotGridDataCell)
        {
            PivotGridDataCell cell = e.Cell as PivotGridDataCell;
            cell.Text = cell.Text.Replace("@", Currency);

            if (cell.Text == "NaN") cell.Text = "";
        }
    }

}
