﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace BrightAffect.Administration
{
    public partial class EditUserRoles : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            try { TextBox_Username.Text = Request.QueryString["User"]; }
            catch { }
        }

        protected void OnClick_NewRole(object sender, EventArgs e)
        {
            RadGrid_Roles.MasterTableView.IsItemInserted = true;
            RadGrid_Roles.Rebind();
        }
    }
}