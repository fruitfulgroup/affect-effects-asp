﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace BrightAffect.API
{
    public partial class GetVideos : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            /*
            if (!FruitfulAPI.IsTokenValid(Request.Form["Username"], Request.Form["Token"]))
            {
                Response.Write(string.Format("{{ \"error\" : \"Login Access is Invalid\" }}"));
                return;
            }
            */

            try
            {
                //Make Connection to Database
                SqlConnection conn = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString);
                SqlDataAdapter adpt = new SqlDataAdapter();
                SqlCommand cmd = new SqlCommand("GetVideoList", conn);

                DataTable dt = new DataTable();

                conn.Open();

                cmd.Parameters.Add(new SqlParameter("@ClientID", Request.Form["ClientID"]));
                cmd.Parameters.Add(new SqlParameter("@LanguageID", Request.Form["LanguageID"]));

                cmd.CommandType = CommandType.StoredProcedure;
                adpt.SelectCommand = cmd;
                adpt.Fill(dt);
                Response.Write(FruitfulAPI.DataTableToJSONArray(dt));
                cmd.Dispose();
                conn.Close();
            }
            catch { Response.Write(string.Format("{{ \"error\" : \"Database execution error.\" }}")); }
        }

    }
}